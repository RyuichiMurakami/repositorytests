<?php session_start(); $_SESSION = array(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta name="robots" content="noindex" /><!-- クローラーに無視してもらうようにお願いする -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />

  <!-- スタイルシートの読み込み -->
  <link rel ="stylesheet" type="text/css" href="BOOTSTRAP/CSS/bootstrap.min.css" media="all">
  <link rel ="stylesheet" type="text/css" href="CSS/mainStyle.css" media="all">

  <title>TNG社員ログイン画面</title>

  <!-- JQuery・bootstrapのjsファイルの読み込み -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script><!--CDN経由でJQuery読み込み（ver3.4.1）-->
  <script src="BOOTSTRAP/JS/bootstrap.min.js"></script><!--bootstrap.min.jsの読み込み-->
  <script src="BOOTSTRAP/JS/jquery.min.js"></script><!--jquery.min.jsの読み込み-->
  <script src="BOOTSTRAP/JS/popper.min.js"></script><!--popper.min.jsの読み込み-->
</head>

  <?php

    /*-------------------------注意！！-------------------------
      ｜このプログラムはPHP5.3.6以上でUTF-8を使う場合の接続方法です｜
      ---------------------------------------------------------
    */

    //session_start();
    include "PHP/PHPLIB/axs_UICustom.php";  //UIカスタムライブラリーを読み込み

    /*--------------------------その他のPHPファイル--------------------------
      ｜"PHP/regist.php"     //登録・削除等のボタンを押した後に表示するファイル｜
      ｜"PHP/LibReadMe.php"  //ライブラリーに関する補足事項等を記述したファイル｜
      ----------------------------------------------------------------------
    */

    /*------------------------アクセスURL------------------------
      ｜http://localhost/AxxxiSSystem/AxxxiSSystem_ver0.0.1/login.php："login.php"の場所｜
      ｜http://localhost/dashboard/:XAMPP：ダッシュボード         ｜
      ｜http://localhost/phpmyadmin：phpMyAdmin                 ｜
      ----------------------------------------------------------
    */

    try{
      $_SESSION = array();
      //lolipop_datebaseserver_ipget();
      //var_dump(DBNAME_LIST);

      //準備その１：各データベースおよびデータベース以下のテーブルの存在確認（なければ自動生成）
      for($i = 0; $i < count(DBNAME_LIST); $i++) { makeDB(DBNAME_LIST[$i]); }//一般スタッフ用,マネージャー以上用のデータベース（ st_db, ex_db ）を各々生成

      //各データベースにアクセスする際の情報（PDOは定義した瞬間から指定したDBにアクセスしているらしい。なので、最後はDBとの接続を解除してあげる必要がある。）
      //一般スタッフ用のデータベースへアクセス時に使用する変数


      $DBH_ST = new PDO( DB_INFO_ST, DB_USER, DB_PASSWORD,
        array(                                          //array部分は”オプション”を指している。
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,  //エラーが発生したときに、PDOExceptionの例外を投げてくれるようにする。
          PDO::ATTR_EMULATE_PREPARES => false,          //動的プレースホルダ―は初期設定では自動でONになっている。なので、この文により手動で無効に切り替える。
        )
      );
      //マネージャー以上用のデータベースへアクセス時に使用する変数
      $DBH_EX = new PDO( DB_INFO_EX, DB_USER, DB_PASSWORD,
        array(                                          //array部分は”オプション”を指している。
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,  //エラーが発生したときに、PDOExceptionの例外を投げてくれるようにする。
          PDO::ATTR_EMULATE_PREPARES => false,          //動的プレースホルダ―は初期設定では自動でONになっている。なので、この文により手動で無効に切り替える。
        )
      );



      for($j = 0; $j < count(TABLENAME_LIST_ST); $j++) { newTableMake(TABLENAME_LIST_ST[$j], "QUERY_CREATE", $DBH_ST, DB_TABLELIST_ST[$j]); }//一般スタッフ用のテーブルを構築
      //DB_PROCESS(INIT_EMPLOYEE_DATA3, DBNAME_LIST[0], TABLENAME_LIST_ST[0], $DBH_ST,"INSERT_MODE","","");//初期情報付与
      unset($DBH_ST); unset($DBH_EX);//データベースとの接続を解除


      //$_SESSION['chkno2'] = 123456789;

    }catch(PDOException $e){
      header('Content-Type: text/plain; charset=UTF-8', true, 500);
      exit($e->getMessage()); //エラーの内容を吐き出す
    }
  ?>

  <script>
  //BackSpaceキーや、ブラウザの”←”ボタンの無効化
  history.pushState(null, null, null);
  $(window).on("popstate", function (event) {
    if (!event.originalEvent.state) {
      history.pushState(null, null, null);
      return;
    }
  });
  </script>

  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="Content-Style-Type" content="text/css" />

      <title>TNG社員ログイン画面</title>

      <!-- ↓JavaScriptの処理を追加 -->

  </head>

  <script type="text/javascript">
    $(function(){
      $('#test').click(function(){
        if (loginForm.loginID.value == ""){
          alert("IDが入力されていません");    //エラーメッセージを出力
          return false;    //送信ボタン本来の動作をキャンセルします
        }else if(loginForm.loginPW.value == ""){
          alert("パスワードが入力されていません");    //エラーメッセージを出力
          return false;    //送信ボタン本来の動作を実行します
        }
        else{
          <?php
            //データベースへアクセス

            $row = array();//配列　"row"を定義
            $row = getAllData(DB_HOST,DB_USER,DB_PASSWORD,DB_PORT,DBNAME_LIST[0],TABLENAME_LIST_ST[0]);//データベースから全情報を取得し$rowに格納（カラム以下の行における全ての個人データの取得）
            $idArray = Array(); $pwArray = Array();//データベースから取得したID,PWを保存する変数を定義

            for($i = 0; $i < count($row); $i++){ array_push($idArray, $row[$i]['id']); array_push($pwArray, $row[$i]['pw']); }//ID,PW一覧を格納

            //json_encodeでjavascript側で扱える変数へと変換
            $json_idArray = json_encode( $idArray , JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_NUMERIC_CHECK);
            $json_pwArray = json_encode( $pwArray , JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_NUMERIC_CHECK);
            unset($row);//メモリ解放（DBから得られた情報を解放）

          ?>

          var idArray = <?php echo $json_idArray ?>;
          var pwArray = <?php echo $json_pwArray ?>;

          for(i = 0; i < idArray.length; i++){ if(loginForm.loginID.value == idArray[i] && loginForm.loginPW.value == pwArray[i]){ alert("認証に成功しました"); return true; } }
          alert("IDまたはパスワードが間違っています");
          return false;

        }
      });
    });
  </script>

  <body class = "login">
    <div class="container text-white" style="height:100vh">
      <br /><br /><div class="row"><div class="col-md-12 col-md-offset-1"><center>TNG社員ログイン画面</center></div></div><br /><br />
        <!-- フォームでログイン画面 -->
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6 col-md-offset-2">
            <form action="PHP/SYSTEM/employeeInfo.php" name="loginForm">
              <div class="form-group">
                <center><label for="name">ID</label></center>
                <input type="text" class="form-control tac" name="loginID" placeholder="example@example.co.jp"><br />
                <center><label for="name">パスワード</label></center>
                <input type="password" class="form-control tac" name="loginPW" placeholder=""><br /><br />
                <center><input type="submit" class="btn btn-blueVer rounded-pill" value="ログイン" id="test"></center>
              </div>
            </form>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
  </body>

</html>
