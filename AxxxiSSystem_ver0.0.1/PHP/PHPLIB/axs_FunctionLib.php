<?php

include "axs_DefineLib.php";     //変数ライブラリーを読み込み

//////////////////////////////////////////////////////////////////////////
/////////////////////////////////便利関数/////////////////////////////////
/////////////////////////////////////////////////////////////////////////

function br_Macro($brtype, $vcount){ for($i = 0; $i < $vcount; $i++) { echo "<br>\n"; } }//改行を任意の回数（$vcountに格納されている数値分）行う関数
function define_Show($assoArray){ foreach($assoArray as $key=> $value){ echo $key.' is '.$value.BR_WIN; } }//連想行列の中身を画面上（ブラウザ上）に出力する関数
function dumpMemory()//メモリー使用量を調べる関数
{
  static $initialMemoryUse = null;
  if ( $initialMemoryUse === null ) { $initialMemoryUse = memory_get_usage(); }
  var_dump(number_format(memory_get_usage() - $initialMemoryUse));//使用メモリー量表示（消しちゃダメ！　絶対！）
}

//ロリポップのデータベースサーバのIPアドレスを取得する関数
function lolipop_datebaseserver_ipget(){
  echo "test\n";
  $hostArray = array();
  for ( $i = 501; $i <= 603; $i++ ) { $hostArray[] = sprintf( "mysql%03d.phy.lolipop.jp", $i ); }
  for ( $i = 1; $i <= 28; $i++ ) { $hostArray[] = sprintf( "mysql%03d.phy.lolipop.lan", $i ); }
  for ( $i = 101; $i <= 112; $i++ ) { $hostArray[] = sprintf( "mysql%03d.phy.lolipop.lan", $i ); }
  foreach( $hostArray as $hostName ) { $ipAddr = gethostbyname( $hostName ); echo "{$hostName} : {$ipAddr}\n"; }
}

//////////////////////////////////////////////////////////////////////////
/////////////////////////////便利関数ここまで//////////////////////////////
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
/////////////////////////////データベース関連/////////////////////////////
/////////////////////////////////////////////////////////////////////////


///////////////////// データベース接続 /////////////////

//任意のデータベースを作成する際に必要な情報を入力する関数
function dbm_Init(DBMake_Class &$DBM, $src, $user, $pw, $dbname){ $DBM->src = $src; $DBM->user = $user; $DBM->pw = $pw; $DBM->dbname = $dbname; }

//任意のデータベースを作成する関数($srcはデータベースが存在するURL、userはユーザ名、$pwはパスワード、$dbNameは作成したいデータベース名)
function db_Make(DBMake_Class $dbm){
  $first_dbh = new PDO('mysql:host='.$dbm->src, $dbm->user, $dbm->pw);//データベースの格納場所そのものにアクセス
  $str = 'CREATE DATABASE IF NOT EXISTS '.$dbm->dbname;//SQL命令文（データベース作成）を生成
  $first_dbh->exec($str);//初回アクセス時にデータベース（$dbName）がなければ作成する。
  unset($first_dbh);//メモリ解放
}

function makeDB($dbName){ $DBM = new DBMake_Class(); dbm_Init($DBM, DB_HOST, DB_USER, DB_PASSWORD, $dbName); db_Make($DBM); unset($DBM); }//データベースを生成する関数

//複数のデータベースを使用する際に、各データベースの情報をDB専用クラスに格納する関数
function db_Init(DB_Class &$DBclass, $HOst, $USer, $Pw, $SQlport, $DBname){
  $DBclass->host = $HOst;
  $DBclass->user = $USer;
  $DBclass->pw = $Pw;
  $DBclass->sqlport = $SQlport;
  $DBclass->dbname = $DBname;
  $DBclass->dbinfo = "mysql:host=".$HOst.";dbname=".$DBname.";port=".$SQlport.";charset=utf8";
}

//接続先のデータベースに関する基本情報が格納されているクラス　"$DBclass"　の内容を元にして、データベースにアクセスするために必要な情報（PDO）を作成し、その情報そのものを返り値とする。
function pdo_Make(DB_Class &$DBclass){
  $dbo = new PDO($DBclass->dbinfo, $DBclass->user, $DBclass->pw,
    array(                                          /*array部分は”オプション”を指している。*/
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,  /*エラーが発生したときに、PDOExceptionの例外を投げてくれるようにする。*/
      PDO::ATTR_EMULATE_PREPARES => false,          /*動的プレースホルダ―は初期設定では自動でONになっている。なので、この文により手動で無効に切り替える。*/
    )
  );
  return $dbo;
}

//　”$dbh”　に紐付されているDBの直下にあるテーブル　"$tableName"　の全情報を取得し、取得した情報を、参照渡しで引っ張ってきた連想配列　"$row"　に格納する関数。
function db_GetAll_Array(&$row,&$dbh,$tableName){
  $statement = $dbh->query(DB_ALL_SELECT.$tableName);//全ての情報を保存
  $row = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す
  $dbh=null;//データベースの接続アウト
}

//DBから、テーブル名 "tableName" の、カラム名 "$request" の値が "$sname" のデータを取得して返す
function db_Get_Array($dbh,$tableName,$request, $sname){
  $sql = DB_ALL_SELECT.$tableName." WHERE ".$request." = '".$sname."'";
  $statement = $dbh->query($sql);//全ての情報を保存
  $row = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す
  return $row;
}

//データベースにアクセスし、全情報を取得して配列rowに格納
function getAllData($ip,$user,$pw,$sqlPort,$dbName,$tableName){
  $testDB = new DB_Class();                                       //新しいデータベースの情報を格納するクラスを定義＆初期化
  db_Init($testDB, $ip,$user,$pw,$sqlPort,$dbName);     //ここで新しいデータベースの情報をクラスに登録
  $newDB = pdo_Make($testDB);                                     //PDOによりデータベースに接続する情報をここで格納
  $row = array();
  db_GetAll_Array($row,$newDB,$tableName);                      //データベースにアクセスし、全情報を取得して配列rowに格納
  return $row;
}

///////////////////// データベース接続・ここまで /////////////////


///////////////////// データベース操作 /////////////////

//連想配列を通常配列に戻す関数
//例）　$rt['性別'] = "女", $rt['年齢'] = 27 なら、　$val_rt[0] = "女", $val_rt[1] = 27　のようになる。
function RowEcho(&$rt){
  $val_rt = array_values($rt);
  return $val_rt;
}

//テーブル構築
function newTableMake($tableName, $requestQuery, $dbhInfo , $queryInfo){ //引数が　”define”　の場合は値渡し（引数に&を付けない）でないとエラーを吐くみたいです。

  switch($requestQuery){

    //ケース　”QUERY_CREATE”　は、SQL命令で、「新しいテーブルを作りたい」場合に用いられます。
    case "QUERY_CREATE":

    //ここでSQL命令文の前半部と後半部が生成される。
    $sqlFirst = DB_CLEATE."{$tableName}"." ( ";
    $sqlEnd = " ) engine = innodb default charset=utf8;";

    //ここでSQL命令文の中身（どういった項目でどの位の文字数まで許可するか等の設定）が作成される。
    $sqlInsert = "";
    for ($i = 0; $i < count($queryInfo); $i = $i + 2) {
      if($i !== 0) { $sqlInsert = $sqlInsert.", ".$queryInfo[$i]." ".$queryInfo[$i + 1]; }
      else { $sqlInsert = $sqlInsert.$queryInfo[$i]." ".$sqlInsert.$queryInfo[$i + 1]; }
    }

    $sql = $sqlFirst.$sqlInsert.$sqlEnd;//上記の３つの文字列を連結させて、命令文を完成させる。
    $statement = $dbhInfo->query($sql);//SQL命令文を実行
    break;

    case "QUERY_ALL_DELETE":
    echo "QUERY_ALL_DELETE";
    break;

    case "QUERY_PART_DELETE":
    echo "QUERY_PART_DELETE";
    break;

    case "QUERY_INSERT":
    echo "QUERY_INSERT";
    break;

    case "QUERY_SORT":
    echo "QUERY_SORT";
    break;

  }
}

//紐付されたデータベースの直下に存在する様々な名称（テーブル名等）を取得し、参照渡しで引っ張ってきた配列に格納する関数
function get_NameList(&$row,$pdoInfo,$command,$dbName, $tableName){

  switch($command){

    //対象となるDB($dbName)直下に存在する全テーブルのテーブル名を取得し、参照渡しで引っ張ってきた配列に格納
    case "TABLELIST":

    $stmt = $pdoInfo->query('SHOW TABLES');
    while($re = $stmt->fetch(PDO::FETCH_ASSOC)){

      $val = RowEcho($re);//連想配列を普通の配列に変換
      array_push($row,$val[0]);
    }
    break;


    //対象となるDB($dbName)直下に存在する全カラム（項目）のカラム名を取得し、参照渡しで引っ張ってきた配列に格納
    case "COLUMNLIST":

    $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$dbName."' AND TABLE_NAME= '".$tableName."'";
    $stmt = $pdoInfo->query($sql);

    while ( $re = $stmt->fetch(PDO::FETCH_ASSOC) ) {
      $val = RowEcho($re);//連想配列を普通の配列に変換
      array_push($row,$val[0]);//配列　”$row”　に取得した全部のカラム名を格納
    }
    break;


  }
}

//連想配列を通常配列に変換する関数（KEY・DATAのどちらかを配列に変換して返す）
function fromAssoarray_toArray($assoArray, $getType){
  switch($getType){
    case "KEY":
    foreach($assoArray as $key => $value){ $array[] = $key; }
    return $array;
    break;

    case "DATA":
    foreach($assoArray as $key => $value){ $array[] = $value; }
    return $array;
    break;
  }
}

//連想配列に含まれる、登録・更新に必要なKeyやdataそのものを取得して配列として返す関数（登録する・更新する等のコマンド部分やテーブル名を格納した部分等、不必要なものを省いて返す）
function asso_ArrayValueReturn($assoArray,$getType,$command){
  switch($getType){
    case "KEY":
    foreach($assoArray as $key => $value){
      if($value === "登録する" || $value === "更新する" || $key === "tn" || $key === "no"){}//配列　”$row”　に取得した全部のカラム名を格納
      //（”登録する”や”更新する”といったコマンド系、およびテーブル名を格納しているキーの部分は必要ないので省く）
      else{ $arrayValue[] = $key; }
    }
    $DateTime = "registryDatetime";//登録日時を取得
    $UpdateTime = "lastUpdate";//最終更新を取得
    if($command !== "更新する" && $command !== "プロフィールを更新する") array_push($arrayValue,$DateTime);//登録日時を配列に追加（　”更新する”　では登録日時は特に必要が無いので省く）
    array_push($arrayValue,$UpdateTime);//最終更新を配列に追加
    if($command === "プロフィールを更新する") array_push($arrayValue,'no');
    return $arrayValue;
    break;

    case "DATA":
    foreach($assoArray as $key => $value){
      if($value === "登録する" || $value === "更新する" || $key === "tn" || $key === "no"){}//配列　”$row”　に取得した全部のカラム名を格納
        //”登録する”や”更新する”といったコマンド系、およびテーブル名を格納している値の部分は必要ないので省く
      else { $arrayValue[] = $value; }
    }
    date_default_timezone_set('Asia/Tokyo');//日時を日本標準時刻に変更
    $DateTime = date("Y/m/d H:i:s");//登録日時を取得
    if($command !== "更新する" && $command !== "プロフィールを更新する") array_push($arrayValue,$DateTime);//登録日時を配列に追加（　”更新する”　では登録日時は特に必要が無いので省く）
    array_push($arrayValue,$DateTime);//最終更新を配列に追加（１回目の登録時は登録日時と最終更新が一致する）
    if($command === "プロフィールを更新する") { array_push($arrayValue,$assoArray['no']); }
    return $arrayValue;
    break;
  }
}

//配列に含まれている文字列を全て連結させたものを返す関数
function stringConnect($str){
  $strings="";
  for($j = 0; $j < count($str); $j++){
    if($j !== count($str) - 1) $strings = $strings.$str[$j].",";
    else $strings.=$str[$j];
  }
  return $strings;
}

//アップデートに必要なキーを連結する関数（最後の項目を除く）
function updateKeyConnect($str){
  $strings="";
  for($j = 0; $j < count($str) - 1; $j++){
    if($j !== count($str)-2) $strings = $strings.$str[$j]." = :".$str[$j].", ";
    else $strings = $strings.$str[$j]." = :".$str[$j];
  }
  return $strings;
}

//アップデートに必要なキーを連結する関数（全て）
function updateKeyFullConnect($str){
  $strings="";
  for($j = 0; $j < count($str); $j++){
    if($j !== count($str)-1) $strings = $strings.$str[$j]." = :".$str[$j].", ";
    else $strings = $strings.$str[$j]." = :".$str[$j];
  }
  return $strings;
}

//データを登録（INSERT）、または更新（UPDATE）する文を変数に格納する関数（値が空のままのSQL文を作成して返す）
function prepareSQL($tableName, $assoKey, $command){
  $sql = "";//最終的な命令文を格納する変数をここで定義
  switch($command){
    case "登録する":
      $insertValueKey = array();
      for($i = 0; $i < count($assoKey); $i++) { array_push($insertValueKey, ":".$assoKey[$i]); }
      $sql = "";

      $sql_First = "INSERT INTO ".$tableName." "."(";
      $sql_keyName = stringConnect($assoKey);
      $sql_VALUES = ") VALUES (";
      $sql_dataName = stringConnect($insertValueKey);
      $sql_End = ")";

      $sql = $sql_First.$sql_keyName.$sql_VALUES.$sql_dataName.$sql_End;
      return $sql;
      break;

    case "更新する":
      $sql_First = "UPDATE ".$tableName." "."SET ";
      $sql_keyName = updateKeyConnect($assoKey);
      $sql_VALUES = " WHERE ";
      $sql_End = $assoKey[count($assoKey)-1]." = :".$assoKey[count($assoKey)-1];
      $sql = $sql_First.$sql_keyName.$sql_VALUES.$sql_End;
      //echo $sql;
      return $sql;
      break;

    case "プロフィールを更新する":
      $sql_First = "UPDATE ".$tableName." "."SET ";
      $sql_keyName = updateKeyConnect($assoKey);
      $sql_VALUES = " WHERE ";
      $sql_End = "no = :no";
      $sql = $sql_First.$sql_keyName.$sql_VALUES.$sql_End;
      return $sql;
      break;
  }
}

//値を変更するために用いる連想配列を作る関数
function assoArrayMake_FromRequest($assoKey,$assoData,$command){
  $params; $j = 0;
  if($command === "登録する"){ $j = 0; }
  //else if($command === "更新する"){ $j = 2; }
  for($i = $j;$i < count($assoKey); $i++){ $params[$assoKey[$i]] = $assoData[$i]; }
  if($command === "更新する") $params[$assoKey[0]] = $assoData[0];//SQL文で、WHERE以降で用いるnoに関する連想配列部分を付け足す
  return $params;
}

//更新に用いるparamsの値をここで作成
function updateAssoarrayMake($assokey, $assodata){
  $params;
  //$assokeyの中は、$assokey[0] = "no0", $assokey[1] = "column0", $assokey[2] = "value0", $assokey[3] = "no1",・・・
  //・・・, $assokey[count($assokey)-2] = "value" + " [{ count($assokey)-2 }/3] の値", $assokey[count($assokey)-1] = "lastUpdate" となっている。

  //また、$assodataの中は、$assodata[0] = 7(更新対象となる一つ目のno), $assodata[1] = "sex"(更新対象となる一つ目のカラム名), $assodata[2] = "女"(更新後の一つ目のカラム名の値),
  //$assodata[3] = 4(更新対象となる二つ目のno), ・・・, $assokey[count($assokey)-2] ="名前" (更新後の [{ count($assokey)-2 }/3 + 1] つ目のカラム名の値),
  // $assokey[count($assokey)-1] ="最終更新の日時" となっている。

  //故に以下の式で、変更する全てのカラム名（key）と変更後の値（data）を保持し、かつその後ろにカラム名”最終更新”(key)とその値”最終更新日時”が付け足され、さらに
  //その後ろ（最後尾）にWHERE文の後に必要となるカラム名no(key)と変更後の値(data)を付け足した連想配列を作成する事が可能となる。

  $inputMoneyExist = 0;//入金の有無が選択されているか否かを判断する変数で、入金の有無が選択されている場合にはその有無が格納されている配列の番号をここに格納する。
  //この値が0より大きい時は、例外として入金時間も弄らなくてはならない。

  for($i = 0; $i < count($assokey)-1; $i++){
    if($assodata[$i] === "inputMoney") { $inputMoneyExist = $i; }
    if($i%3 === 1){ $params[$assodata[$i]] = $assodata[$i + 1]; }//変更に用いるカラム名（key）,値（data）を順に格納してゆく
  }
  if($inputMoneyExist > 0){
    //入金の有無が有に変更されたら、その情報を$paramsに追加（key => inputMoneyType, data => 最終更新時刻）、なお入金の有無の値は、”入金の有無”というキーの次の番号の配列に格納されている。
    if($assodata[$inputMoneyExist + 1] === "有") $params['inputMoneyTime'] = $assodata[count($assodata)-1];
    else $params['inputMoneyTime'] = "";//入金の有無が無に変更されたら入金時刻をリセットする。
  }
  $params[$assokey[count($assokey)-1]] = $assodata[count($assodata)-1];//カラム名”最終更新”(key)とその値（最終更新日時）(data)を、連想配列の最後尾に追加
  $params['no'] = $assodata[0];//WHERE文の後に必要となるカラム名no(key)と、その中身となる変更後の値（data）をここで格納
  return $params;
}

function DB_PROCESS($request, $dbName, $tableName, $dbhInfo, $processMode, $seed, $selectColumn){

  if(!isset($request['PHPSESSID'])) { unset($request['PHPSESSID']); }//何か勝手に入ってることがあるので削除（ある日突然入るようになった、いったい何が起こってるの？）

  switch($processMode){



    //データ挿入処理
    case "INSERT_MODE":

      //ここでまず、今選択されているテーブルに登録されている全情報を取得して表示させる。
      $mode = "登録する";
      $dbhInfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。
      $asso_key = asso_ArrayValueReturn($request,"KEY",$mode);//連想配列に格納されているキー群を配列に格納
      $asso_data = asso_ArrayValueReturn($request,"DATA",$mode);//連想配列に格納されているデータ群を配列に格納(各項目の値を配列で格納)
      $params = assoArrayMake_FromRequest($asso_key,$asso_data,$mode);
      $sql2 = prepareSQL($tableName, $asso_key,$mode );//sql命令文を作成して格納
      $stmt2 = $dbhInfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする
      $stmt2->execute($params);// 挿入する値が入った連想配列　"$params"　をexecuteにセットしてSQLを実行
      break;

    //データ更新処理
    case "UPDATE_MODE":

      //ここでまず、今選択されているテーブルに登録されている全情報を取得して表示させる。
      $dbhInfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。

      if($seed == "default" || $seed == "profile" || $seed = "attendance"){
        $mode = "";
        if($seed == "default") { $mode = "更新する"; }
        else { $mode = "プロフィールを更新する"; }
        $asso_key = asso_ArrayValueReturn($request,"KEY",$mode);//連想配列に格納されているキー群を配列に格納
        $asso_data = asso_ArrayValueReturn($request,"DATA",$mode);//連想配列に格納されているデータ群を配列に格納(各項目の値を配列で格納)
        //var_dump($asso_key);
        //var_dump($asso_data);
        if($seed == "default") { updateAssoarrayMake($asso_key, $asso_data); } else { for($i = 0; $i < count($asso_key); $i++){ $params[$asso_key[$i]] = $asso_data[$i]; } } //値を更新する際に用いる連想配列を予め作成しておく
        //var_dump($params);
        $assoKey = fromAssoarray_toArray($params, "KEY");//連想配列 "$params" のキーを配列に変換し、 "$assoKey" に格納
        $sql2 = prepareSQL($tableName,$assoKey,$mode);//sql命令文を作成して格納
        //echo $sql2;
        //var_dump($sql2);
      }

      else if($seed == "terms"){
        $conditions = $request['conditions0'];//条件（no or id）
        $condValue = $request['condvalue0'];//条件の値
        $selectColumn = $request['column0'];//選択されたカラム名（KEY）
        $setColumnValue = $request['value0'];//更新後のカラムの値
        date_default_timezone_set('Asia/Tokyo');//日時を日本標準時刻に変更
        $now = date("Y/m/d H:i:s");//現在日時を取得（最終更新に入る）
        $params = array(":".$selectColumn => $setColumnValue,":lastUpdate" => $now,":".$conditions => $condValue );//値を更新する際に用いる連想配列を作成
        $sql2 = "UPDATE $tableName SET ".$selectColumn." = :".$selectColumn.", lastUpdate = :lastUpdate WHERE ".$conditions." = :".$conditions;//sql命令文を作成して格納
      }

      $stmt2 = $dbhInfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする。プレースホルダ―（値を入れるための単なる空箱）を用意し、値が空のままのSQL文をセット
      $stmt2->execute($params);// 挿入する値が入った連想配列　"$params"　をexecuteにセットしてSQLを実行
      break;

      //データ検索処理
      case "SEARCH_MODE":

        //事前準備
        $dbhInfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。
        $needColumn = "";
        if($selectColumn != "") {
          $needColumn.= " ".$selectColumn[0];
          for($i = 1; $i < count($selectColumn); $i++){ $needColumn.= ", ".$selectColumn[$i]; }
        }
        else{$needColumn = " *"; }
        $sql2 = "SELECT".$needColumn." FROM ".$tableName." WHERE ";//初めの条件文をここで指定

        for($i = 0; $i < count($request['searchOption']); $i++){
          $st = $request['searchType'][$i];//完全一致か部分一致かをここに格納

          if(count($request['searchOption']) == 1){//そもそも検索条件が一つしかない場合
            if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
            elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
            elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
            elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}
          }

          else{
            if($request['searchOption'][$i] == "全体的AND条件" || $request['searchOption'][$i] == "全体的OR条件") { $type = "ALL"; }
            else {$type = "PART";}

            if($request['searchOption'][$i] == "全体的AND条件" || $request['searchOption'][$i] == "部分的AND条件") { $searchType = "AND"; }
            if($request['searchOption'][$i] == "全体的OR条件" || $request['searchOption'][$i] == "部分的OR条件") { $searchType = "OR"; }

            if($i == 0){
              if($request['searchOption'][$i] == "部分的AND条件" || $request['searchOption'][$i] == "部分的OR条件"){ $sql2 .= "( "; }
              else if($request['searchOption'][$i + 1] == "部分的AND条件" || $request['searchOption'][$i + 1] == "部分的OR条件"){ $sql2 .= "( "; }

              if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
              elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
              elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
              elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}
            }

            else if($i == count($request['searchOption']) - 1){

              if($request['searchOption'][$i-1] == "全体的AND条件" || $request['searchOption'][$i-1] == "全体的OR条件") { $type_pre = "ALL"; }
              else {$type_pre = "PART";}

              if($type_pre == "PART" && $type == "ALL"){
                if(mb_substr($sql2, -1) != ")" ) { $sql2 .= " ) "; }
                $sql2 .= $searchType." ";
              }
              else {$sql2 .= " ".$searchType." ";}

              if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
              elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
              elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
              elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}

              if($type_pre == "ALL" && $type == "PART"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; } }
              if($type_pre == "PART" && $type == "PART"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; } }
            }

            else{

              if($request['searchOption'][$i-1] == "全体的AND条件" || $request['searchOption'][$i-1] == "全体的OR条件") { $type_pre = "ALL"; }
              else {$type_pre = "PART";}

              if($request['searchOption'][$i+1] == "全体的AND条件" || $request['searchOption'][$i+1] == "全体的OR条件") { $type_post = "ALL"; }
              else {$type_post = "PART";}

              if($type_pre == "PART" && $type == "ALL" && $type_post == "ALL"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; }  }
              if($type_pre == "PART" && $type == "ALL" && $type_post == "PART"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; } }

              $sql2.= " ".$searchType." ";
              if($type_pre == "ALL" && $type == "ALL" && $type_post == "PART"){ if(mb_substr($sql2, -2) != "( " ) { $sql2.="( "; } }
              if($type_pre == "PART" && $type == "ALL" && $type_post == "PART"){ if(mb_substr($sql2, -2) != "( " ) { $sql2.="( "; } }

              if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
              elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
              elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
              elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}

              if($type_pre == "ALL" && $type == "PART" && $type_post == "ALL"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; }  }
              if($type_pre == "PART" && $type == "PART" && $type_post == "ALL"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; }  }
            }
          }
        }

        //var_dump($sql2);

        $rowRow = array();
        $statement = $dbhInfo->query($sql2);//全ての情報を保存
        $rowRow = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す
        return $rowRow;
        break;

      //データ削除処理
      case "DELETE_MODE":

        if($seed == "default" || $seed == "terms"){
          $mode = "";
          if($seed == "default") { $sql ="DELETE FROM $tableName WHERE no = :no"; } else { $conditions = $request['condition']; $sql ="DELETE FROM $tableName WHERE ".$conditions." = :".$conditions; }//DELETE文を変数に格納
          $stmt = $dbhInfo->prepare($sql);// 削除するレコードのNOは空のまま、SQL実行の準備をする
          if($seed == "default") { $params = array(':no' => $request['del_no']); } //削除するレコードのNOを、連想配列 "$params" に格納する( 実質的には・・・ $params[':no'] = $_REQUEST['del_no']; )
          else { $params = array(":".$conditions => $request['del_no']); }
          $stmt->execute($params);// 削除するレコードのNOが入った変数をexecuteにセットしてSQLを実行
        }
        break;
  }
}

//////////////////// データベース操作・ここまで //////////////////


////////////////////////////////////////////////////////////////
//////////////////// データベース関連・ここまで //////////////////
////////////////////////////////////////////////////////////////


/////////////////////////////////デバッグ用/////////////////////////////////
//バックトレースを使用し、ソースを直接読み、関数を呼び出している行に存在する２次元連想配列（DBからデータを引っ張ってきてforeachで配列に格納するとこのタイプの配列になる）
//の配列名を取得する関数を応用したデータ確認兼デバッグ用の関数。var_dump単体でも十分わかりやすいが、こちらは２次元連想配列の配列名まで記述される。
//(注)　引っ張ってきた２次元連想配列にデータが入ってないと当然データがみれないので注意してください・・・(^_^;)
function debug_r($assoArray){
    $trace = debug_backtrace();
    $file = $trace[0]['file'];//ファイルの場所（いわゆる "path" のこと）
    $line = $trace[0]['line'];//行数
    $vLine = file( $file );//?
    $fLine = $vLine[$line-1];//?
    preg_match( "/\\$\w+/", $fLine, $match );//?
    $varname = (string)$match[0];//変数名を取得

    $k = 0; $l = 0; $m = 0;
    if(isset($assoArray[$k])) { $l = count($assoArray[$k]); $m = count($assoArray); }//$l = 項目の数（カラム数）,$m = 行数

    echo br_Macro("WIN", 3);
    echo "ファイルパス： ".$file."　行数： ".$line;
    echo br_Macro("WIN", 1);

    echo "配列名： ".$varname."　　カラム数： ".$l."　　行数： ".$m;
    echo br_Macro("WIN", 2);

    for($i = 0; $i < $m; $i++){//count($assoArray) = 行数（テーブル内に保存されているデータの行数、人の個人データ等であれば、DBに登録されている人数と等しい）
      $key = asso_ArrayValueReturn($assoArray[$i],"KEY");
      $data = asso_ArrayValueReturn($assoArray[$i],"DATA");
      for($j = 0; $j < $l; $j++){//$l = 項目の数（カラム数）
        if(gettype($data[$i * $k + $j]) === "string"){ echo $varname."[".$i."]"."['".$key[$i * $k + $j]."']"." = "."'".$data[$i * $k + $j]."'".br_Macro("WIN", 1); }
        else{ echo $varname."[".$i."]"."['".$key[$i * $k + $j]."']"." = ".$data[$i * $k + $j].br_Macro("WIN", 1); }
      }
      echo br_Macro("WIN", 2);
    }
}
/////////////////////////////////デバッグ用・ここまで/////////////////////////////////



////////////////////////////////////////////////////////////////
///////////////// ウェブサイト・プロフィール画像更新///////////////
////////////////////////////////////////////////////////////////

//ファイルをサーバにアップロードする際に用いる関数
function profileIMGUpload($id,$fileTmpPath,$filePath,&$bool2,&$message,$setW,$setH,$setQ){
  //filepath　:　$_FILES['upfile']['name']のこと
  //$fileTmpPath　:　$_FILES['upfile']['tmp_name']のこと

  if(isset($_REQUEST['profileImageUpdate'])){
    if($_REQUEST['profileImageUpdate'] === "更新"){

      $f_name = '../../EMPLOYEE_IMG/';

        //画像アップロード処理
        $bool2 = true;
        $image = $f_name.$id;//ファイル名をユーザのIDに変更
        $type = '.'.substr(strrchr($filePath, '.'), 1);//アップロードされたファイルの拡張子を取得
        $image2 = $image.$type;//ファイル名（ID）＋拡張子（.jpg）を、そのデータ（画像）のファイル名とする

        $NW = $setW; $NH = $setH; $Quality = $setQ;
        list($width, $height) = getimagesize($fileTmpPath); // 元の画像名を指定してサイズを取得

        if($width > $setW && $height > $setH){
          $part_w = $setW / $width; $part_h = $setH / $height;
          if($part_w <= $part_h) { $newWidth = $setW; $newHeight = $height * $part_w; }
          else{ $newWidth = $setW * $part_h; $newHeight = $setH; }
        }
        else if($width > $setW && $height <= $setH){ $part_w = $setW / $width; $newWidth = $setW; $newHeight = $height * $part_w; }
        else if($width <= $setW && $height > $setH){ $part_h = $setH / $height; $newWidth = $setW * $part_h; $newHeight = $setH; }
        else{ $newWidth = $width; $newHeight = $height; }

        $ReImg = imagecreatetruecolor($newWidth, $newHeight); // サイズを指定して新しい画像のキャンバスを作成
        $newIMG = $f_name.$id.'.jpg';

        chmod($f_name, 0777);//フォルダー自体のアクセス権限を変更（全ての権限を与える）
        if(file_exists($newIMG)) {
          chmod($newIMG, 0777);//このファイルに対するアクセス権限を変更（全ての権限を与える）
          unlink($newIMG);//もしすでにプロフィール画像がある場合は削除してからアップロード
        }

        if($type == '.jpg' || $type == '.jpeg'){
          $baseImage = imagecreatefromjpeg($fileTmpPath); // 元の画像から新しい画像を作る準備
        }

        else if($type == '.png' || $type == '.gif'){
          if($type == '.png') { $baseImage = imagecreatefrompng($fileTmpPath); }// 元の画像から新しい画像を作る準備(png)
          else if($type == '.gif') { $baseImage = imagecreatefromgif($fileTmpPath); }// 元の画像から新しい画像を作る準備(gif)
          imagealphablending($ReImg, false);//ブレンドモードを無効にする
          imagesavealpha($ReImg, true);//完全なアルファチャネル情報を保存するフラグをonにする
          //よくわからないけどphpは変数名の使い回しはできないらしい（バグる）
        }

        imagecopyresampled($ReImg, $baseImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);// 画像のコピーと伸縮
        imagejpeg($ReImg , $newIMG, $Quality);// コピーした画像をjpegに変換して出力する
        chmod($newIMG, 0604);//このファイルに対するアクセス権限を変更（全ての権限を与える）
        imagedestroy($ReImg);// メモリを開放します
        imagedestroy($baseImage);// メモリを開放します

        $type = '.jpg';
        if (!empty($filePath)) {//ファイルが選択されていればOK
          if (is_uploaded_file($fileTmpPath)) {//is_uploaded_file:アップロードされたファイルかどうか調べる
            if($type != ".jpg"){ $message = 'jpg画像ではないため、アップロードできませんでした。'; }
            else{ $message = 'アップロードが完了しました。'; }
          }
          else { $message = 'ファイルのアップロード先が不明の為、アップロードできませんでした。'; }
        }
        else{ $message = 'ファイルが選択されていません。'; }
    }
  }
}

function checkBoxImplode(&$request){
  $temp = array('transportation','favoriteSubject','availableDays','teachingLevel');
  for($i = 0; $i < count($temp); $i++){ if(isset($request[$temp[$i]])) { $request[$temp[$i]] = implode("・", $request[$temp[$i]]); } }
  //豆知識：文字列を選択したまま　Shift + '　キーを押すと、なんと　'文字列'　にできる。（たまたま見つけた）
}

function monthcheck($filename){
  if(!is_array($filename)) { return false; var_dump("not array"); }
  $bool = false;
  $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  for($i = 0; $i < count($month); $i++ ){ if( $filename[5] == $month[$i]) { $bool = true; } }
  return $bool;
}

function filenameCheck($filename){
  $errorCode = "";
  if(!is_array($filename)) { $errorCode = "error : ファイル名が正しくありません。"; return $errorCode;}
  if(count($filename) != 6) { $errorCode = "error : 「_」で区切られる数が正しくありません。"; return $errorCode;}
  if($filename[1] != "ICT" && $filename[1] != "ITM" && $filename[1] != "PM" && $filename[1] != "ITP" && $filename[1] != "ITS" && $filename[1] != "OFFICER") { $errorCode = "所属部署が正しくありません。"; return $errorCode; }
  if($filename[2] != "ITST" && $filename[2] != "ST" && $filename[2] != "MGR" && $filename[2] != "EX") { $errorCode = "error : 役職が正しくありません。"; return $errorCode; }
  if($filename[3] != "payslip") { return false; echo "error : このファイルは給与明細ファイルではない可能性があります。"; return $errorCode; }
  if(!monthcheck($filename)) { $errorCode = "error : 給与月が正しくありません。"; return $errorCode; }
  return $errorCode;
}

?>
