<?php session_start(); ?>
<?php
  header("Cache-Control:no-cache,no-store,must-revalidate,max-age=0");
  header("Cache-Control:pre-check=0","post-check=0",false);
  header("Pragma:no-cache");
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png"><!-- スマホとかのタッチアイコン？ -->
  <link rel="icon" type="image/png" href="../assets/img/favicon.png"><!-- PCでタブの横にでてくるアレ -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="robots" content="noindex" /><!-- クローラーに無視してもらうようにお願いする -->
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /><!-- レスポンシブ対応 -->

  <!-- JQuery・チャート・カラーパレット読み込み -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script><!--CDN経由でJQuery読み込み（ver3.4.1）-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <script type="text/javascript" src="https://github.com/nagix/chartjs-plugin-colorschemes/releases/download/v0.2.0/chartjs-plugin-colorschemes.min.js"></script>

  <!-- clndr.jsを使用可能にするためのライブラリの読み込み -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

  <title>Axxxis System</title>

  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Files -->
  <link href="../../BOOTSTRAP/CSS/bootstrap.min.css" rel="stylesheet" />
  <link href="../../CSS/mainStyle.css" rel="stylesheet" />
</head>

<?php
  /*-------------------------注意！！-------------------------
    ｜このプログラムはPHP5.3.6以上でUTF-8を使う場合の接続方法です｜
    ---------------------------------------------------------
  */
  //session_start();
  include "../PHPLIB/axs_UICustom.php";  //UIカスタムライブラリーを読み込み



  /*--------------------------その他のPHPファイル--------------------------
    ｜"PHP/regist.php"     //登録・削除等のボタンを押した後に表示するファイル｜
    ｜"PHP/LibReadMe.php"  //ライブラリーに関する補足事項等を記述したファイル｜
    ----------------------------------------------------------------------
  */

  /*---------------------アクセスURL---------------------
    ｜http://localhost/AxxxiSSystem/PHP/SYSTEM/employeeInfo.php："employeeInfo.php"の場所              ｜
    ｜http://localhost/dashboard/:XAMPP：ダッシュボード  ｜
    ｜http://localhost/phpmyadmin：phpMyAdmin          ｜
    ----------------------------------------------------
  */

  try{
    $bool = false;

    //事前準備
    $dbName = DBNAME_LIST[0];//接続するDB名をここで定義
    $DBC = new DB_Class();//新しいDBクラスを定義
    db_Init($DBC,DB_HOST,DB_USER,DB_PASSWORD,DB_PORT,$dbName);//DB情報を変更
    $NewPDO = pdo_Make($DBC);//DBに接続する為のPDOを生成
    $tableName = TABLENAME_LIST_ST[1];//attendanceinfo。接続するテーブル名をここで定義。これをやっておかないと、以下の!isset($_REQUEST['dbQuery'])にいれると２回目以降に無定義状態になるらしい。

    //希望休承認処理
    if(isset($_REQUEST['attendanceOK'])){
      //var_dump($_REQUEST);
      if($_REQUEST['attendanceOK'] === "上記の内容を承認する"){

        $rowRow = $_SESSION['row'];
        //var_dump($rowRow);
        if(!empty($rowRow)){
          $tableName = TABLENAME_LIST_ST[1];//"customerinfo"

          $sql = "UPDATE ".$tableName." SET mgrOK = :mgrOK WHERE ( accessType = 'ST' OR accessType = 'MGR' ) AND department = '".$_SESSION['DM']."' AND year = '".$_SESSION['row'][0]['year']."' AND month = '".$_SESSION['row'][0]['month']."'";
          //var_dump($sql);
          $stmt = $NewPDO->prepare($sql);// 挿入する値は空のまま、SQL実行の準備をする。プレースホルダ―（値を入れるための単なる空箱）を用意し、値が空のままのSQL文をセット
          $params = array('mgrOK' => 'OK');
          $stmt->execute($params);
          $bool = true;
        }
      }

    }

    $_SESSION["chkno"] = $chkno = mt_rand();//新しい照合番号をセット

    //プロフィール更新処理
    if(isset($_REQUEST['attendanceCheck'])){
      if($_REQUEST['attendanceCheck'] === "上記の内容で希望休承認を行う"){

        unset($_REQUEST['attendanceCheck']);

        //データベースへアクセス
        $sql = "SELECT * FROM ".$tableName." WHERE id = "."'".$_SESSION['ID']."'";
        $statement = $NewPDO->query($sql);//全ての情報を保存
        $row = array();//配列　"row"を定義
        $row = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す

        $tableName = TABLENAME_LIST_ST[1];//"attendanceinfo"
        $y = $_REQUEST['year']; $m = $_REQUEST['month'];//西暦・月
        $dbName = DBNAME_LIST[0];


        $dataList = [
          'tn' => $tableName,
          'searchOption' => array('部分的AND条件','部分的OR条件','全体的AND条件','全体的AND条件','全体的AND条件'),
          'column' => array('accessType','accessType','department','year','month'),
          'setValue' => array('ST', 'MGR',$_SESSION['DM'],$y,$m),
          'searchType' => array('である','である','である','である','である')
        ];

        /*
        $dataList = [
          'tn' => $tableName,
          'searchOption' => array('全体的AND条件','全体的AND条件'),
          'column' => array('year','month'),
          'setValue' => array($y,$m),
          'searchType' => array('である','である')
        ];
        */

        $showColumn = array("name","nameRuby",'year','month');
        $row = DB_PROCESS($dataList, $dbName, $tableName, $NewPDO, 'SEARCH_MODE', '', $showColumn);
        //var_dump($row);
        //$row = searchExe($dataList, $dbName, $tableName, $FIRST_DBH, $showColumn);//対象講師の授業年-授業月の授業報告日程を全て取得

      }
    }
    //プロフィール更新処理終了

  }catch(PDOException $e){
    header('Content-Type: text/plain; charset=UTF-8', true, 500);
    exit($e->getMessage()); //エラーの内容を吐き出す
  }
?>

<script>
//page topボタン
$(function(){

  //事前準備
  var userAgent = window.navigator.userAgent.toLowerCase();//使用しているブラウザを調べる
  var interF = "";
  if(userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1) { interF = "IE"; }
  else if(userAgent.indexOf('edge') != -1) { interF = "Edge"; }
  else if(userAgent.indexOf('chrome') != -1) { interF = "Chrome"; }
  else if(userAgent.indexOf('safari') != -1) { interF = "Safari"; }
  else if(userAgent.indexOf('firefox') != -1) { interF = "firefox"; }
  else if(userAgent.indexOf('opera') != -1) { interF = "opera"; }
  var y = 0;
  var targetElement = document.getElementById( "temp" ) ;
  var clientRect = targetElement.getBoundingClientRect() ;
  var max_y = clientRect.top ;// 画面内の位置
  var pagetop=$('#target');
  pagetop.hide();

  setInterval(function(){

    if(interF == "Safari"){
      var y = window.pageYOffset;
      //alert(y);
      if(y > 300) { pagetop.fadeIn(); }
      else { pagetop.fadeOut(); }
    }
    else{
      targetElement = document.getElementById( "temp" ) ;
      clientRect = targetElement.getBoundingClientRect() ;
      y = clientRect.top ;// 画面内の位置
      var py = max_y - clientRect.top ;// ページ内の位置
      //console.log(py);
      if(py > 300) { pagetop.fadeIn(); /*$('.footer' + '.fixed-bottom').css('display','');*/ }
      else { pagetop.fadeOut(); }
    }
  },1000);

  //$(".sidebar-dropdown3 > a").removeClass("active");
  //$(".sidebar-dropdown3 > a").next(".sidebar-submenu3").slideDown(200);

});
</script>

<body class="user-profile">



<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#"><i class="fas fa-bars"></i></a>

  <!--sidebar-->
  <?php sidebarMake("希望休承認",$_SESSION); ?>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      <div class="wrapper">
        <div class="main-panel" id="main-panel">

          <!-- End Navbar -->
          <div class="panel-header panel-header-sm"></div>

          <div class="content">
            <div class="row">

              <div class="col-md-12">
                <div class="card">
                  <div class="card-header"><h5 class="title">シフト閲覧・出力</h5></div>
                  <div class="card-body">

                    <form action="./shiftTableApproval.php" name="loginForm">

                      <div class="row">
                        <?php profileMake("year", "","4","","対象年","対象年（西暦）を入力して下さい。","","","",""); ?>
                        <?php profileMakeDate("month", "4", "", "対象月"); ?>
                      </div>

                      <br />
                      <center><input type="submit" class = "btn btn-blueVer rounded-pill " name="attendanceCheck" value="上記の内容で希望休承認を行う"></center>
                      <?php
                        if(isset($row)) {
                          outputTableParts($tableName,$row,$showColumn); //出力テーブル（検索結果等を表示する部分の作成）
                          echo '<center><input type="submit" class = "btn btn-blueVer rounded-pill" name="attendanceOK" value="上記の内容を承認する"></center>';
                          $_SESSION['row'] = $row;
                        }
                      ?>
                    </form>
                    <?php if(isset($row)) { echo '<div id="temp" style="height:20vh;"></div>'; } ?>
                    <?php if($bool == true) { echo '<br /><center><p>送信が完了しました</p></center>'; } ?>

                  </div>
                </div>
              </div>

            </div>
          </div>

          <div id="temp" style="height:20vh;"></div>
          <footer class="footer fixed-bottom" id = "target" style="display:none;">
            <div class=" container-fluid ">
              <nav style="float:right;">
                <a href="#main-panel" style="float:right;"><i class="now-ui-icons arrows-1_minimal-up btn btn-orangeVer rounded-pill"></i></a>
              </nav>
            </div>
          </footer>

        </div>
      </div>

    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->


  <!--   Core JS Files   -->
  <script src="../../BOOTSTRAP/JS/jquery.min.js"></script>
  <script src="../../BOOTSTRAP/JS/popper.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap-notify.js"></script>
  <script src="../../BOOTSTRAP/JS/sidebar.js"></script>
  <script src="../../BOOTSTRAP/JS/clndr.js"></script>
  <script type="text/javascript">
    $('#clndr').clndr();
  </script>

</body>
</html>
