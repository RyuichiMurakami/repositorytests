<?php

include "axs_FunctionLib.php";     //関数ライブラリーを読み込み

function customInputType($value,$column,$size){ return $value.'： <br /><input type="text" name="'.$column.'" size="'.$size.'vw" value=""><br /><br />'; }
function customInputType2($value,$column,$size){ return $value.'： <br /><input type="text" name="'.$column.'" id = "nt" size="'.$size.'vw" value="" readonly><br /><br />'; }
function customInputType3($value,$column,$size){ return $value.'： <br /><input type="date" name="'.$column.'"value=""><br /><br />'; }
function customInputType4($value,$column,$size){ return $value.'： <br /><input type="time" name="'.$column.'"value=""><br /><br />'; }
function customInputType5($value,$column,$size){ return $value.'： <br /><input type="tel" name="'.$column.'" size="'.$size.'vw" value=""><br /><br />'; }
function customInputType6($value,$column,$size){ return $value.'： <br /><input type="date" name="'.$column.'" id = "BD" value=""><br /><br />'; }
function customInputType7($value,$column,$size){ return $value.'： <br /><input type="text" name="'.$column.'" id = "AG" value=""><br /><br />'; }

function optionValueSet($value, $firstSelect){
  $result='';
  if($firstSelect === 'YES') { $result = '<option value="">'.$value.'</option>'; }
  else { $result = '<option value="'.$value.'">'.$value.'</option>'; }
  return $result;
 }

 function optionValueSetDisabled($value, $firstSelect){
   $result='';
   if($firstSelect === 'YES') { $result = '<option value="" disabled>'.$value.'</option>'; }
   else { $result = '<option value="'.$value.'" disabled>'.$value.'</option>'; }
   return $result;
  }

 function optionValueSetRead($value, $firstSelect){
   $result='';
   if($firstSelect === 'YES') { $result = '<option value="" disabled>'.$value.'</option>'; }
   else { $result = '<option value="'.$value.'" disabled>'.$value.'</option>'; }
   return $result;
  }

 function optionValueSetInt($valueMin, $valueMax){
   $result = '<option value="">選択してください</option>';
   for($i = $valueMin; $i <= $valueMax; $i++){ $result .= '<option value="'.$i.'">'.$i.'</option>'; }
   return $result;
  }

 function optionValueSetSelect($value, $firstSelect){
   $result='';
   if($firstSelect === 'YES') { $result = '<option value="'.$value.'" selected>'.$value.'</option>'; }
   else { $result = '<option value="'.$value.'" selected>'.$value.'</option>'; }
   return $result;
  }

function optionValueSetType2($key, $value, $firstSelect){
   $result='';
   if($firstSelect === 'YES') { $result = '<option value="">'.$value.'</option>'; }
   else { $result = '<option value="'.$key.'">'.$value.'</option>'; }
   return $result;
  }

function optionValueSetType3($key, $value, $firstSelect){
    $result='';
    if($firstSelect === 'YES') { $result = '<option value="">'.$value.'</option>'; }
    else {
      if($value !== 'NO' && $value !== '登録日時' && $value !== '最終更新')
       $result = '<option value="'.$key.'">'.$value.'</option>';
     }
    return $result;
}

//講師情報
/*
define'TEACHER_INFO_ASSOARRAY'
  'birthday' => '生年月日',//完全新規項目
  'zipCode' => '郵便番号',//追加項目
  'address' => '住所',//追加項目
  'telNumberHome'=> '電話番号（自宅）',//順序変更項目・項目名等変更項目（電話番号からの変質）
  'telNumberMobile'=> '電話番号（携帯）',//完全新規項目
  'telNumberEmergency'=> '電話番号（緊急連絡先）',//完全新規項目
  'email' => 'Email',//追加項目
  'seInfoGet' => 'SEからのお知らせ',//完全新規項目
  'education' =>'学歴',//項目名等変更項目（最終学歴からの変質）
  'qualification' => '資格・免許',//順序変更項目・項目名等変更項目（資格からの変質）
  'transportation' => '交通手段',//項目名等変更項目（車所持からの変質）
  'alongLine' => '沿線',//完全新規項目
  'teachingHistory' => '指導履歴',//項目名等変更項目（指導歴からの変質）
  'favoriteSubject' => '指導可能科目',//順序変更項目
  'availableDays' => '指導可能曜日',//完全新規項目
  'teachingLevel' => '指導可能レベル',//完全新規項目
  'successRecord' => '合格実績',//順序変更項目
  'selfAppeal' => '自己アピール',//順序変更項目
  'travelTime' => '移動時間',//完全新規項目
  'desiredWorkRegion' => '勤務地の希望',//完全新規項目
  'allianceSchoolIntro' => '提携塾案件の紹介',//完全新規項目
  'opportunityKnowingTrigger' => 'SEを知ったきっかけ'//完全新規項目
*/

function columnFormMake($column){

  //htmlタグを作成する為に使用する変数
  $selectFirst = '<br /><select name="'.$column.'" >';
  $selectEnd = '</select><br /><br />';

  switch($column){
    case 'id': echo customInputType("ID",$column,30); break;//IDを入力するフォームの作成
    case 'tID': echo customInputType("講師ID",$column,30); break;//講師IDを入力するフォームの作成
    case 'sID': echo customInputType("担当生徒ID",$column,30); break;//担当生徒IDを入力するフォームの作成
    case 'pw': echo customInputType("パスワード",$column,30); break;//氏名を入力するフォームの作成
    case 'name': echo customInputType("名前",$column,30); break;//氏名を入力するフォームの作成
    case 'tName': echo customInputType("講師名",$column,30); break;//講師名を入力するフォームの作成
    case 'sName': echo customInputType("担当生徒名",$column,30); break;//担当生徒名を入力するフォームの作成
    case 'nameRuby': echo customInputType("フリガナ",$column,50); break;//氏名を入力するフォームの作成
    case 'tNameRuby': echo customInputType("講師名（フリガナ）",$column,50); break;//講師名（フリガナ）を入力するフォームの作成
    case 'sNameRuby': echo customInputType("担当生徒名（フリガナ）",$column,50); break;//担当生徒名（フリガナ）を入力するフォームの作成
    case 'monthGoal': echo customInputType("月間目標",$column,70); break;//月間目標を入力するフォームの作成

    case 'sex':
      echo '性別：'.$selectFirst
                    .optionValueSet("選択してください", "YES")
                    .optionValueSet("男", "")
                    .optionValueSet("女", "")
                  .$selectEnd; break;//性別を選択するフォームの作成

    case 'age': echo customInputType7("年齢",$column,10); break;//年齢を入力するフォームの作成
    case 'fromWhere': echo customInputType("県・市",$column,70); break;//県・市を入力するフォームの作成
    case 'education': echo customInputType("学歴",$column,70); break;//学歴を入力するフォームの作成
    case 'favoriteSubject': echo customInputType("指導可能科目",$column,70); break;//指導可能科目を入力するフォームの作成
    case 'availableDays': echo customInputType("指導可能曜日",$column,70); break;//指導可能曜日を入力するフォームの作成
    case 'teachingLevel': echo customInputType("指導可能レベル",$column,70); break;//指導可能レベルを入力するフォームの作成
    case 'successRecord': echo customInputType("合格実績",$column,70); break;//合格実績を入力するフォームの作成
    case 'selfAppeal': echo customInputType("自己アピール",$column,70); break;//自己アピールを入力するフォームの作成
    case 'qualification': echo customInputType("資格／免許",$column,70); break;//資格／免許を入力するフォームの作成
    case 'teachingHistory': echo customInputType("指導履歴",$column,70); break;//指導履歴を入力するフォームの作成

    case 'travelTime':
      echo '移動時間：'.$selectFirst
                        .optionValueSet("選択してください", "YES")
                        .optionValueSet("遠くても良い", "")
                        .optionValueSet("40分以内", "")
                        .optionValueSet("20分以内", "")
                      .$selectEnd; break;//移動時間を選択するフォームの作成

    case 'desiredWorkRegion': echo customInputType("勤務地の希望",$column,70); break;//勤務地の希望を入力するフォームの作成

    case 'allianceSchoolIntro':
      echo '提携塾案件の紹介：'.$selectFirst
                                .optionValueSet("選択してください", "YES")
                                .optionValueSet("はい", "")
                                .optionValueSet("いいえ", "")
                              .$selectEnd; break;//提携塾案件の紹介を受けるかうけないかを選択するフォームの作成

    case 'opportunityKnowingTrigger': echo customInputType("SEを知ったきっかけ",$column,70); break;//SEを知ったきっかけを入力するフォームの作成
    case 'birthday': echo customInputType6("生年月日",$column,50); break;//生年月日を入力するフォームの作成
    case 'zipCode': echo customInputType("郵便番号",$column,50); break;//郵便番号を入力するフォームの作成
    case 'address': echo customInputType("住所",$column,70); break;//住所を入力するフォームの作成
    case 'telNumber': echo customInputType5("電話番号",$column,50); break;//電話番号を入力するフォームの作成
    case 'telNumberHome': echo customInputType5("電話番号（自宅）",$column,50); break;//電話番号（自宅）を入力するフォームの作成
    case 'telNumberMobile': echo customInputType5("電話番号（携帯）",$column,50); break;//電話番号（携帯）を入力するフォームの作成
    case 'telNumberEmergency': echo customInputType5("電話番号（緊急連絡先）",$column,50); break;//電話番号（緊急連絡先）を入力するフォームの作成
    case 'email': echo customInputType("Email",$column,70); break;//Emailを入力するフォームの作成

    case 'seInfoGet':
      echo 'SEからのお知らせ：'.$selectFirst
                                .optionValueSet("選択してください", "YES")
                                .optionValueSet("受け取る", "")
                                .optionValueSet("受け取らない", "")
                              .$selectEnd; break;//SEからのお知らせを受け取るか受け取らないかを選択するフォームの作成

    case 'transportation': echo customInputType("交通手段",$column,70); break;//交通手段を入力するフォームの作成
    case 'alongLine': echo customInputType("沿線",$column,70); break;//沿線を入力するフォームの作成

    case 'course':
    echo '受講コース：'.$selectFirst
                      .optionValueSet("選択してください", "YES")
                      .optionValueSet("基礎学力コース（小）", "")
                      .optionValueSet("中学校受験コース", "")
                      .optionValueSet("基礎学力コース（中）", "")
                      .optionValueSet("高校受験コース", "")
                      .optionValueSet("通常コース", "")
                      .optionValueSet("大学受験コース", "")
                      .optionValueSet("社会人・浪人生コース", "")
                    .$selectEnd; break;//受講コースを選択するフォームの作成

    case 'goal': echo customInputType("目標",$column,70); break;//目標を入力するフォームの作成
    case 'aspiringSchool': echo customInputType("志望校",$column,70); break;//志望校を入力するフォームの作成
    case 'attendanceTime': echo customInputType4("出勤時刻",$column,50); break;//出勤時刻を入力するフォームの作成
    case 'homecomingTime': echo customInputType4("退勤時刻",$column,50); break;//退勤時刻を入力するフォームの作成
    case 'dateInfo': echo customInputType3("日付",$column,50); break;//日付を入力するフォームの作成

    case 'studentCheck':
      echo '生徒確認：'.$selectFirst
                        .optionValueSet("選択してください", "YES")
                        .optionValueSet("OK", "")
                        .optionValueSet("NG", "")
                      .$selectEnd; break;//生徒確認を選択するフォームの作成

    case 'schoolName': echo customInputType("学校名",$column,70); break;//学校名を入力するフォームの作成

    case 'academicCode':
    echo '学校種別：'.$selectFirst
                      .optionValueSet("選択してください", "YES")
                      .optionValueSet("小", "")
                      .optionValueSet("中", "")
                      .optionValueSet("高", "")
                      .optionValueSet("社浪", "")
                    .$selectEnd; break;//学校種別を選択するフォームの作成

    case 'grade': echo customInputType("学年",$column,20); break;//学年を入力するフォームの作成
    case 'testType': echo customInputType("テストの種類",$column,70); break;//テストの種類を入力するフォームの作成
    case 'arithmeticGrade': echo customInputType("算数",$column,20); break;//テストの点数（算数）を入力するフォームの作成
    case 'languageGrade': echo customInputType("国語",$column,20); break;//テストの点数（国語）を入力するフォームの作成
    case 'scienceGrade': echo customInputType("理科",$column,20); break;//テストの点数（理科）を入力するフォームの作成
    case 'societyGrade': echo customInputType("社会",$column,20); break;//テストの点数（社会）を入力するフォームの作成
    case 'englishGrade': echo customInputType("英語",$column,20); break;//テストの点数（英語）を入力するフォームの作成
    case 'mathGrade': echo customInputType("数学",$column,20); break;//テストの点数（数学）を入力するフォームの作成
    case 'math1AGrade': echo customInputType("数ⅠA",$column,20); break;//テストの点数（数ⅠA）を入力するフォームの作成
    case 'math2BGrade': echo customInputType("数ⅡB",$column,20); break;//テストの点数（数ⅡB）を入力するフォームの作成
    case 'math3CGrade': echo customInputType("数ⅢC",$column,20); break;//テストの点数（数ⅢC）を入力するフォームの作成
    case 'modernLiteratureGrade': echo customInputType("現代文",$column,20); break;//テストの点数（現代文）を入力するフォームの作成
    case 'oldLiteratureGrade': echo customInputType("古文",$column,20); break;//テストの点数（古文）を入力するフォームの作成
    case 'chineseLiteratureGrade': echo customInputType("漢文",$column,20); break;//テストの点数（漢文）を入力するフォームの作成
    case 'essayGrade': echo customInputType("小論文",$column,20); break;//テストの点数（小論文）を入力するフォームの作成
    case 'chemistryGrade': echo customInputType("化学",$column,20); break;//テストの点数（化学）を入力するフォームの作成
    case 'physicsGrade': echo customInputType("物理",$column,20); break;//テストの点数（物理）を入力するフォームの作成
    case 'biologyGrade': echo customInputType("生物",$column,20); break;//テストの点数（生物）を入力するフォームの作成
    case 'geologyGrade': echo customInputType("地学",$column,20); break;//テストの点数（地学）を入力するフォームの作成
    case 'geographyGrade': echo customInputType("地理",$column,20); break;//テストの点数（地理）を入力するフォームの作成
    case 'jpnHistoryGrade': echo customInputType("日本史",$column,20); break;//テストの点数（日本史）を入力するフォームの作成
    case 'worldHistoryGrade': echo customInputType("世界史",$column,20); break;//テストの点数（世界史）を入力するフォームの作成
    case 'ethicsGrade': echo customInputType("倫理",$column,20); break;//テストの点数（倫理）を入力するフォームの作成
    case 'politicalEconomicsGrade': echo customInputType("政経",$column,20); break;//テストの点数（政経）を入力するフォームの作成
    case 'modernSocietyGrade': echo customInputType("現代社会",$column,20); break;//テストの点数（現代社会）を入力するフォームの作成
    case 'psTeacherReport': echo customInputType("教師レポート",$column,70); break;//教師レポートを入力するフォームの作成
    case 'inputMoney':
      echo '入金の有無：'.$selectFirst
                          .optionValueSet("選択してください", "YES")
                          .optionValueSet("有", "")
                          .optionValueSet("無", "")
                        .$selectEnd; break;//入金の有無を選択するフォームの作成

    case 'inputMoneyTime': echo customInputType2("入金完了時刻",$column,18); break;//指導歴を入力するフォームの作成

  }
}

function inputValueSet($value){ echo '<input type="submit" class="btn btn-orangeVer rounded-pill" name = "dbQuery" value="'.$value.'" /></form><br /><br /><br /><br />'; }
function inputValueSet2($value){ echo '<table width="100%"><th width="100%"><input type="submit" class="btn btn-orangeVer rounded-pill" name = "dbQuery" value="'.$value.'"/></th></table></form><br />'; }
function inputSearch($value,$type){ return '<input type="button" class="col-md-12 btn btn-orangeVer rounded-pill" name = "'.$type.'" value="'.$value.'" id="search'.$type.'"/>'; }

function changedColumn($columnList){
  $row = array();
  for($i = 0;$i < count($columnList);$i++){
    $columnName = '';
    switch($columnList[$i]){
      case 'no': $columnName = 'no'; break;
      case 'name': $columnName = '名前'; break;
    }
    array_push($row,$columnName);
  }
  return $row;
}

function addSearchForm($tableName,$count){
  $selectTab = "";
  $selectFirst = '<select name="column['.$count.']" id="column" class="CO" required>';
  $selectTypeFirst = '<select name="searchType['.$count.']" id="searchType" class="ST" required>';
  $selectCenter = '';
  $selectEnd = '</select>';

  $selectCenter = '<div id="searchMenu"><select name = "searchOption['.$count.']" id="searchOption" class="SO" required>'
  .optionValueSet("選択してください", "YES")
  .optionValueSet("全体的AND条件", "")
  .optionValueSet("全体的OR条件", "")
  .optionValueSet("部分的AND条件", "")
  .optionValueSet("部分的OR条件", "")
  .$selectEnd
  .'カラム名'.$selectFirst.optionValueSet("選択してください", "YES");

  //optionタグ出力（新しいテーブルを追加したらここにその情報を追加してください）
  switch($tableName){
    case TABLENAME_LIST[0]: foreach(TEACHER_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[1]: foreach(CUSTOMER_INPUT_MONEY_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[2]: foreach(PROSPECT_CUSTOMER_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[3]: foreach(CUSTOMER_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[4]: foreach(PS_GRADE_MANAGEMENT_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[5]: foreach(JHS_GRADE_MANAGEMENT_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[6]: foreach(STUDENT_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[7]: foreach(MASTER_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[8]: foreach(CHARGE_STUDENT_INFO_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
    case TABLENAME_LIST[9]: foreach(HS_GRADE_MANAGEMENT_ASSOARRAY as $key => $r){ $selectCenter.=optionValueSetType2($key, $r, ""); } break;
  }

  $selectTab = $selectCenter.$selectEnd;//カラム名を選択するフォームの作成
  $selectTab.='の値が<input type="text" name="setValue['.$count.']" id="setValue" class="SV" size="30" value="">';
  $selectTab.=$selectTypeFirst
  .optionValueSet("選択してください", "YES")
  .optionValueSet("である", "")//選択肢（完全一致）
  .optionValueSet("で始まる", "")//選択肢（前方一致）
  .optionValueSet("で終わる", "")//選択肢（後方一致）
  .optionValueSet("を含む", "")//選択肢（部分一致）
  .$selectEnd.inputSearch("検索条件を追加","add").inputSearch("検索条件を削除","sub").'</div>';
  $t = inputSearch("検索条件を追加","add").inputSearch("検索条件を削除","sub").'</div>';
  return $selectTab;
}

function typeFormMake($tableName,$columnList,$count,$type){

  switch($type){
    case "INPUT_FORM":
      echo '<br /><br /><br /><br /><form method="post" style="display: inline">登録フォーム：<br /><br /><br />';
              for($i = 1; $i < count($columnList)-1; $i++) { columnFormMake($columnList[$i]); }
              echo 'テーブル名：<input type="text" name="tn" size="20" value="'.$tableName.'" readonly style="display: inline"><br /><br />';
              inputValueSet("登録する");
      break;

    case "DELETE_FORM":
      echo '<form method="post" style="display: inline">削除フォーム：<br /><br /><br />';
      echo 'テーブル名：<input type="text" name="tn" size="20" value="'.$tableName.'" readonly><br /><br />';
        echo 'NO：<input type="int" name="del_no" size="30" value="">の情報を';
              inputValueSet("削除する");
      break;

    case "SEARCH_FORM":
      echo '<form method="post" style="display: inline">検索フォーム：<br /><br /><br />';
      echo 'テーブル名：<input type="text" name="tn" size="20" value="'.$tableName.'" readonly><br /><br />';
      echo addSearchForm($tableName,$count);
      inputValueSet("検索する");
      break;

    case "UPDATE_FORM":
      $selectFirst = '<select name="column0" required>';
      $selectEnd = '</select>';
      echo '<form method="post">更新フォーム：<br /><br /><br />';
      echo 'テーブル名：<input type="text" name="tn" size="20" value="'.$tableName.'" readonly><br /><br />';
        echo 'NO<input type="text" name="no0" size="30" value="">の';
        echo 'カラム名'.$selectFirst.optionValueSet("選択してください", "YES");

        //optionタグ出力（新しいテーブルを追加したらここにその情報を追加してください）
        switch($tableName){
          case TABLENAME_LIST[0]: foreach(TEACHER_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[1]: foreach(CUSTOMER_INPUT_MONEY_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[2]: foreach(PROSPECT_CUSTOMER_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[3]: foreach(CUSTOMER_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[4]: foreach(PS_GRADE_MANAGEMENT_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[5]: foreach(JHS_GRADE_MANAGEMENT_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[6]: foreach(STUDENT_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[7]: foreach(MASTER_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[8]: foreach(CHARGE_STUDENT_INFO_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
          case TABLENAME_LIST[9]: foreach(HS_GRADE_MANAGEMENT_ASSOARRAY as $key => $r){ echo optionValueSetType3($key, $r, ""); } break;
        }

        echo $selectEnd.'の値を<input type="text" name="value0" size="30" value="">に';
        inputValueSet("更新する");
      break;
  }
}



function inputFormMake($tableName,$columnList,$count){
  echo '<table width="100%" class="allConnect2">'
        .'<tr>'
          .'<td class="operate">'
            .'<div class = "inputInfo">';
                typeFormMake($tableName,$columnList,$count,"INPUT_FORM");
                typeFormMake($tableName,$columnList,$count,"UPDATE_FORM");
                typeFormMake($tableName,$columnList,$count,"DELETE_FORM");
                typeFormMake($tableName,$columnList,$count,"SEARCH_FORM");
        echo '</div>'
          .'</td>'
        .'</tr>'
      .'</table>';
}

function inputFormMakeParts($tableName,$columnList,$count,$createForm){
  echo '<div class = "inputInfo">';
                if($createForm == "INPUT_FORM") typeFormMake($tableName,$columnList,$count,"INPUT_FORM");
                else if($createForm == "UPDATE_FORM") typeFormMake($tableName,$columnList,$count,"UPDATE_FORM");
                else if($createForm == "DELETE_FORM") typeFormMake($tableName,$columnList,$count,"DELETE_FORM");
                else if($createForm == "SEARCH_FORM") typeFormMake($tableName,$columnList,$count,"SEARCH_FORM");
  echo '</div>';
}

function outputColumnShow($tableName){
  //選択したテーブルの全カラムをここで表示させる。新しくテーブルを追加したら、ここに新しいcaseとして追加してください。
  switch($tableName){
    case TABLENAME_LIST[0]: foreach(TEACHER_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[1]: foreach(CUSTOMER_INPUT_MONEY_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[2]: foreach(PROSPECT_CUSTOMER_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[3]: foreach(CUSTOMER_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</td>'; } break;
    case TABLENAME_LIST[4]: foreach(PS_GRADE_MANAGEMENT_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[5]: foreach(JHS_GRADE_MANAGEMENT_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[6]: foreach(STUDENT_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[7]: foreach(MASTER_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[8]: foreach(CHARGE_STUDENT_INFO_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
    case TABLENAME_LIST[9]: foreach(HS_GRADE_MANAGEMENT_ASSOARRAY as $r){ echo '<th>',$r,'</th>'; } break;
  }
}

function outputDataShow($rowData){
  if(isset($rowData)){
    foreach($rowData as $key => $rt){
      $val = RowEcho($rt);//連想配列を普通の配列に変換
      echo '<tr>';
      for($i = 0; $i < count($val); $i++)
      {
        echo '<td>'.$val[$i].'</td>';
      }
      echo '</tr>';
    }
  }
}

function outputTableParts($tableName,$rowData, $showColumn){
  echo '<br />';
  echo '<div class="table-responsive">'
        .'<table class="table table-striped text-center">'
          .'<tbody>'
              .'<tr">';
                  foreach(EMPLOYEE_ATTENDANCE_INFO_ASSOARRAY as $key => $r){
                    for($j = 0; $j < count($showColumn); $j++){
                      if($key == $showColumn[$j]) { echo '<td>',$r,'</td>'; }
                    }
                  }
          echo '</tr>';
            outputDataShow($rowData);
        echo '</tbody>'
        .'</table>'
      .'<div>';

}

function outputTableMake2($tableName,$rowData){
  echo '<div class="row">'
        .'<div class="col-md-12" style="max-height:40vh; overflow-x:scroll; ">'
          .'<table class="allConnect table table-striped">'
            .'<tbody>'
              .'<tr>';
                  outputColumnShow($tableName);
                  outputDataShow($rowData);
          echo '</tr>'
            .'</tbody>'
          .'</table>'
        .'</div>'
      .'</div>';
}

function outputTableMake($tableName,$rowData){
  echo '<table width="100%" class="allConnect table table-striped">'
        .'<tr>'
          .'<td class="show">'
            .'<div class = "showInfo">'
              .'<table class="showConnect">'
                .'<tr>';
                    outputColumnShow($tableName);
                    outputDataShow($rowData);
            echo '</tr>'
              .'</table>'
            .'</div>'
         .'</td>'
        .'</tr>'
      .'</table>';
}

function tableListShowMake($tableName,$request,$row){
  echo '<form method="post" name="tableChange" style="text-align: center">'
        .'対象テーブル名を'
          .'<select name="dbSelect">';
          //配列rowRowに格納していた各テーブルをプルダウン方式で選択できるようにする。
              $fDBName = $tableName;
              if(!$request['dbQuery']){
                for($i = 0; $i < count($row); $i++) {
                  if($row[$i] === $fDBName ){ echo "<option value=".$row[$i]." selected>".$row[$i]."</option>"; }
                  else { echo "<option value=".$row[$i].">".$row[$i]."</option>"; }
                }
              }
              else{
                for($i = 0; $i < count($row); $i++) {
                  if($row[$i] === $request['dbSelect']){$fDBName = $request['dbSelect']; echo "<option value=".$row[$i]." selected>".$row[$i]."</option>"; }
                  else { echo "<option value=".$row[$i].">".$row[$i]."</option>"; }
                }
              }
        echo'</select>'
        .'<input type="submit" class="btn btn-orangeVer rounded-pill" name = "dbChange" value="に変更する" />'.'
        </form>';
  echo "<p name =".' "nowDB" '."class=".' "non" '.">現在選択中のテーブル名： ".$fDBName."</p>";//現在選択しているテーブル名をここで表示
}

function profileMake2($name, $readType, $mdValue, $type, $label, $placeholder, $value, $textStyle, $rows, $cols){
  $textType = "text";
  if($name == "dateInfo"){
    $textType = "date";
  }
  else if($name == "attendanceTime" || $name == "homecomingTime") { $textType = "time"; }

  switch($textStyle){
    case "":
      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label>'
                .'<input type = "'.$textType.'" id = "'.$name.'" class = "form-control" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.'>'
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label>'
                .'<input type = "'.$textType.'" id = "'.$name.'" class = "form-control" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.'>'
              .'</div>'
            .'</div>';
      }
      break;


    case "FREE":
      echo '<div class = "col-md-'.$mdValue.'">'
            .'<div class="form-group">'
              .'<label>'.$label.'</label>'
              .'<textarea rows= "'.$rows.'" cols="'.$cols.'" id = "'.$name.'" class = "form-control" style="border-radius: 0px" name="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.'>'.$value.'</textarea>'
            .'</div>'
          .'</div>';
      break;
  }
}

function profileMake($name, $readType, $mdValue, $type, $label, $placeholder, $value, $textStyle, $rows, $cols){
  $textType = "text";
if($name == "dateInfo"/* || $name == "birthday"*/){
    $textType = "date";
  }
  else if($name == "attendanceTime" || $name == "homecomingTime") { $textType = "time"; }

  if($name == "inputMoneyTime") {$id = ' id = "nt"'; }
  else if($name == "birthday") {$id = ' id = "BD"'; }
  else if($name == "age") {$id = ' id = "AG"'; }
  else {$id = ""; }

  switch($textStyle){
    case "":
      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label>'
                .'<input type = "'.$textType.'"'.$id.' class = "form-control" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.'>'
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label>'
                .'<input type = "'.$textType.'"'.$id.' class = "form-control" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.'>'
              .'</div>'
            .'</div>';
      }
      break;


    case "FREE":
      echo '<div class = "col-md-'.$mdValue.'">'
            .'<div class="form-group">'
              .'<label>'.$label.'</label>'
              .'<textarea rows= "'.$rows.'" cols="'.$cols.'" class = "form-control" style="border-radius: 0px" name="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.'>'.$value.'</textarea>'
            .'</div>'
          .'</div>';
      break;
  }
}

function profileMakeEX($name, $id,$class,$readType, $mdValue, $type, $label, $placeholder, $value, $textStyle, $rows, $cols,$req){
  $textType = "text";
  if($name == "dateInfo"){
    $textType = "date";
  }
  else if($name == "attendanceTime" || $name == "homecomingTime") { $textType = "time"; }

  switch($textStyle){
    case "":
      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label>'
                .'<input type = "'.$textType.'" id="'.$id.'" class = "form-control '.$class.'" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.' '.$req.'>'
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label>'
                .'<input type = "'.$textType.'" id="'.$id.'" class = "form-control '.$class.'" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.' '.$req.'>'
              .'</div>'
            .'</div>';
      }
      break;


    case "FREE":
      echo '<div class = "col-md-'.$mdValue.'">'
            .'<div class="form-group">'
              .'<label>'.$label.'</label>'
              .'<textarea rows= "'.$rows.'" cols="'.$cols.'" id="'.$id.'" class = "form-control '.$class.'" style="border-radius: 0px" name="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" '.$readType.' '.$req.'>'.$value.'</textarea>'
            .'</div>'
          .'</div>';
      break;
  }
}

function profileMakeSelections($name, $mdValue,$label,$selectData){

  $selectFirst = '<select name="'.$name.'" class="form-control" style="border-radius: 0px" required>';
  $selectEnd = '</select>';
  $echoStr = $selectFirst.optionValueSet("選択してください", "YES");
  for($i = 0; $i < count($selectData); $i++ ){ $echoStr .= optionValueSet($selectData[$i], ""); }
  $echoStr .= $selectEnd; //任意のデータセットから一つのデータを選択するフォームの作成
  $textType = "text";
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
}

function profileMakeSelectionsEX($name, $id, $class, $mdValue,$label,$selectData,$req){

  $selectFirst = '<select name="'.$name.'" id = "'.$id.'" class="form-control '.$class.'" style="border-radius: 0px" '.$req.'>';
  $selectEnd = '</select>';
  $echoStr = $selectFirst.optionValueSet("選択してください", "YES");
  for($i = 0; $i < count($selectData); $i++ ){ $echoStr .= optionValueSet($selectData[$i], ""); }
  $echoStr .= $selectEnd; //任意のデータセットから一つのデータを選択するフォームの作成
  $textType = "text";
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
}

function profileMakeSelections2($name, $mdValue,$label,$selectKey, $selectData){

  $selectFirst = '<select name="'.$name.'" class="form-control" style="border-radius: 0px" >';
  $selectEnd = '</select>';
  $echoStr = $selectFirst.optionValueSet("選択してください", "YES");
  for($i = 0; $i < count($selectData); $i++ ){ $echoStr .= optionValueSetType2($selectKey[$i], $selectData[$i], ""); }
  $echoStr .= $selectEnd; //任意のデータセットから一つのデータを選択するフォームの作成
  $textType = "text";
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
}

function profileMakeSelections2EX($name, $id, $class, $mdValue,$label,$selectKey, $selectData,$req){

  $selectFirst = '<select name="'.$name.'" id = ".$id." class="form-control '.$class.'" style="border-radius: 0px" '.$req.'>';
  $selectEnd = '</select>';
  $echoStr = $selectFirst.optionValueSet("選択してください", "YES");
  for($i = 0; $i < count($selectData); $i++ ){ $echoStr .= optionValueSetType2($selectKey[$i], $selectData[$i], ""); }
  $echoStr .= $selectEnd; //任意のデータセットから一つのデータを選択するフォームの作成
  $textType = "text";
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
}

function profileMakeDate($name, $mdValue, $type, $label){

  $selectFirst = '<select name="'.$name.'" class="form-control" style="border-radius: 0px" >';
  $selectEnd = '</select>';
  $echoStr = $selectFirst
  .optionValueSet("選択してください", "YES")
  .optionValueSet("01", "").optionValueSet("02", "").optionValueSet("03", "").optionValueSet("04", "")
  .optionValueSet("05", "").optionValueSet("06", "").optionValueSet("07", "").optionValueSet("08", "")
  .optionValueSet("09", "").optionValueSet("10", "").optionValueSet("11", "").optionValueSet("12", "")
  .$selectEnd; //月を選択するフォームの作成
  $textType = "text";

      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
      }
}

function profileMakePulldownMode($name, $mode, $mdValue, $type, $label,$optionValueData, $selectValue, $command){

  $com = "";
  if($command == "required") {$com = "required"; }
  $selectFirst = '<select name="'.$name.'" class="form-control" style="border-radius: 0px" '.$com.'>';
  $selectEnd = '</select>';

  if($optionValueData == null) $tempCount = 0;
  else $tempCount = count($optionValueData);

  $echoStr = $selectFirst;
  //$echoStr = $selectFirst.optionValueSet("", "YES");
  for($i = 0; $i < $tempCount; $i++){

    //var_dump($optionValueData[$i]);
    //var_dump($selectValue);

    if($selectValue != $optionValueData[$i]) { if($mode != "readonly") { $echoStr .= optionValueSet($optionValueData[$i], ""); } else { $echoStr .= optionValueSetDisabled($optionValueData[$i], ""); } }
    else { $echoStr .= optionValueSetSelect($optionValueData[$i], ""); }
  }
  $echoStr .= $selectEnd;

  $textType = "text";
      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
      }
}

function profileCheckBoxMakeEX($name, $mdValue, $label, $selectData, $req, $sliceNum){
  //<input type="checkbox" name="teacherSubjPS[]" id="teacherSubj01" value = "国語（小）"/>
  echo '<div class = "col-md-'.$mdValue.'"><div class = "form-group">'.'<label>'.$label.'</label><br />';
  echo '<table class = "allConnect3">';
  for($i = 0; $i < count($selectData); $i++){
    if($i % $sliceNum == 0) echo "<tr>";
    $tempID = "";
    if($i < 10) { $tempID = $name."0".$i; } else { $tempID = $name.$i; }
    //if($i > 0) echo "　　　　";
    echo '<td><input type = "checkbox" name = "'.$name.'[]" id = "'.$tempID.'" value = "'.$selectData[$i].'" '.$req.'>';
    echo '　'.'<label for="'.$tempID.'">'.$selectData[$i].'</label></td>';
    if($i % $sliceNum == $sliceNum - 1 || $i == count($selectData) - 1) echo "</tr>";
  }
  echo '</table></div></div>';
}

function entryFormMake($column){
  for($h = 0; $h <= 100; $h++){ $scoreValue[] = $h; }
  $columnMD = 4;
  switch($column){
    case 'id': profileMake("id", "","12", "", "ID", "example@example.ne.jp", "","","",""); break;//IDを入力するフォームの作成
    case 'tID': profileMake("tID", "","12", "", "講師ID", "○○@△△.com", "","","",""); break;//講師IDを入力するフォームの作成
    case 'sID': profileMake("sID", "","12", "", "担当生徒ID", "□□@××.ne.jp", "","","",""); break;//担当生徒IDを入力するフォームの作成
    case 'pw': profileMake("pw", "","12", "", "パスワード", "", "","","",""); break;//パスワードを入力するフォームの作成
    case 'name': profileMake("name", "","12", "", "名前", "田中太郎", "","","",""); break;//名前を入力するフォームの作成
    case 'tName': profileMake("tName", "","12", "", "講師名", "佐藤次郎", "","","",""); break;//講師名を入力するフォームの作成
    case 'sName': profileMake("sName", "","12", "", "担当生徒名", "鈴木三郎", "","","",""); break;//担当生徒名を入力するフォームの作成
    case 'nameRuby': profileMake("nameRuby", "","12", "", "フリガナ", "タナカタロウ", "","","",""); break;//フリガナを入力するフォームの作成
    case 'tNameRuby': profileMake("tNameRuby", "","12", "", "講師名（フリガナ）", "サトウジロウ", "","","",""); break;//講師名（フリガナ）を入力するフォームの作成
    case 'sNameRuby': profileMake("sNameRuby", "","12", "", "担当生徒名（フリガナ）", "スズキサブロウ", "","","",""); break;//担当生徒名（フリガナ）を入力するフォームの作成
    case 'monthGoal': profileMake("monthGoal", "","12", "", "月間目標", "学年3位", "","","",""); break;//月間目標を入力するフォームの作成
    case 'age': profileMake("age", "","12", "", "年齢", "23", "","","",""); break;//年齢を入力するフォームの作成
    case 'education': profileMake("education", "","12", "", "学歴", "○○大学△△学部××学科　卒", "","FREE","4","80"); break;//学歴を入力するフォームの作成
    //case 'favoriteSubject': profileMake("favoriteSubject", "","12", "", "指導可能科目", "数学・理科（中）", "","FREE","4","80"); break;//指導可能科目を入力するフォームの作成
    //case 'availableDays': profileMake("availableDays", "","12", "", "指導可能曜日", "月・日", "","","",""); break;//指導可能曜日を入力するフォームの作成
    //case 'teachingLevel': profileMake("teachingLevel", "","12", "", "指導可能レベル", "成績最上位・上位", "","","",""); break;//指導可能レベルを入力するフォームの作成
    case 'successRecord': profileMake("successRecord", "","12", "", "合格実績", "小学2年生のA君を名門の○○中学校に入学させることができました。", "","FREE","4","80"); break;//合格実績を入力するフォームの作成
    case 'selfAppeal': profileMake("selfAppeal", "","12", "", "自己アピール", "よろしくお願いします。", "","FREE","4","80"); break;//自己アピールを入力するフォームの作成
    case 'qualification': profileMake("qualification", "","12", "", "資格／免許", "○○年△月　普通車第一種免許（MT）取得・英検2級・TOEIC: 800点…", "","FREE","4","80"); break;//資格／免許を入力するフォームの作成
    case 'teachingHistory': profileMake("teachingHistory", "","12", "", "指導履歴", "家庭教師2年・塾講師3年…", "","FREE","2","80"); break;//指導履歴を入力するフォームの作成
    case 'desiredWorkRegion': profileMake("desiredWorkRegion", "","12", "", "勤務地の希望", "東京都23区内", "","FREE","2","80"); break;//勤務地の希望を入力するフォームの作成
    case 'opportunityKnowingTrigger': profileMake("opportunityKnowingTrigger", "","12", "", "SEを知ったきっかけ", "チラシ", "","","",""); break;//SEを知ったきっかけを入力するフォームの作成
    case 'birthday': profileMake("birthday", "","12", "", "生年月日", "1991-08-15", "","","",""); break;//生年月日を入力するフォームの作成
    case 'zipCode': profileMake("zipCode", "","12", "", "郵便番号", "XXX-XXXX", "","","",""); break;//郵便番号を入力するフォームの作成
    case 'address': profileMake("address", "","12", "", "住所", "○○県△△市××町X-X-XX　□□マンション　XXX号室", "","FREE","4","80"); break;//住所を入力するフォームの作成
    case 'telNumber': profileMake("telNumber", "","12", "", "電話番号", "XXX-XXX-XXXX", "","","",""); break;//電話番号を入力するフォームの作成
    case 'telNumberHome': profileMake("telNumberHome", "","12", "", "電話番号（自宅）", "XXX-XXX-XXXX", "","","",""); break;//電話番号（自宅）を入力するフォームの作成
    case 'telNumberMobile': profileMake("telNumberMobile", "","12", "", "電話番号（携帯）", "090-XXXX-XXXX", "","","",""); break;//電話番号（携帯）を入力するフォームの作成
    case 'telNumberEmergency': profileMake("telNumberEmergency", "","12", "", "電話番号（緊急連絡先）", "042-XXX-XXXX", "","","",""); break;//電話番号（緊急連絡先）を入力するフォームの作成
    case 'email': profileMake("email", "","12", "", "Email", "○○.ne.jp", "","","",""); break;//Emailを入力するフォームの作成
    //case 'transportation': profileMake("transportation", "","12", "", "交通手段", "自転車・バイク", "","","",""); break;//交通手段を入力するフォームの作成
    case 'alongLine': profileMake("alongLine", "","12", "", "沿線", "自宅最寄駅：　○○線　△△駅", "","FREE","2","80"); break;//沿線を入力するフォームの作成
    case 'goal': profileMake("goal", "","12", "", "目標", "○○中学合格", "","FREE","4","80"); break;//目標を入力するフォームの作成
    case 'aspiringSchool': profileMake("aspiringSchool", "","12", "", "志望校", "第一志望校：○○高等学校　英数特進", "","FREE","4","80"); break;//志望校を入力するフォームの作成
    case 'attendanceTime': profileMake("attendanceTime", "","12", "", "出勤時刻", "08:15", "","","",""); break;//出勤時刻を入力するフォームの作成
    case 'homecomingTime': profileMake("homecomingTime", "","12", "", "退勤時刻", "14:30", "","","",""); break;//退勤時刻を入力するフォームの作成
    case 'dateInfo': profileMake("dateInfo", "","12", "", "日付（授業日）", "2018-03-15", "","","",""); break;//日付（授業日）を入力するフォームの作成
    case 'schoolName': profileMake("schoolName", "","12", "", "学校名", "○○市立△△小学校", "","","",""); break;//学校名を入力するフォームの作成


    case 'grade': profileMake("grade", "","12", "", "学年", "3", "","","",""); break;//学年を入力するフォームの作成
    case 'testType': profileMake("testType", "","12", "", "テストの種類", "駿台模試（12月）", "","","",""); break;//テストの種類を入力するフォームの作成


    case 'arithmeticGrade': profileMakeStudentMode("arithmeticGrade",$columnMD, "","算数",$scoreValue,-1,""); break;//テストの点数（算数）を入力するフォームの作成
    case 'languageGrade': profileMakeStudentMode("languageGrade",$columnMD, "","国語",$scoreValue,-1,""); break;//テストの点数（国語）を入力するフォームの作成
    case 'scienceGrade': profileMakeStudentMode("scienceGrade",$columnMD, "","理科",$scoreValue,-1,""); break;//テストの点数（理科）を入力するフォームの作成
    case 'societyGrade': profileMakeStudentMode("societyGrade",$columnMD, "","社会",$scoreValue,-1,""); break;//テストの点数（社会）を入力するフォームの作成
    case 'englishGrade': profileMakeStudentMode("englishGrade",$columnMD, "","英語",$scoreValue,-1,""); break;//テストの点数（英語）を入力するフォームの作成
    case 'mathGrade': profileMakeStudentMode("mathGrade",$columnMD, "","数学",$scoreValue,-1,""); break;//テストの点数（数学）を入力するフォームの作成
    case 'math1AGrade': profileMakeStudentMode("math1AGrade",$columnMD, "","数ⅠA",$scoreValue,-1,""); break;//テストの点数（数ⅠA）を入力するフォームの作成
    case 'math2BGrade': profileMakeStudentMode("math2BGrade",$columnMD, "","数ⅡB",$scoreValue,-1,""); break;//テストの点数（数ⅡB）を入力するフォームの作成
    case 'math3CGrade': profileMakeStudentMode("math3CGrade",$columnMD, "","数ⅢC",$scoreValue,-1,""); break;//テストの点数（数ⅢC）を入力するフォームの作成
    case 'modernLiteratureGrade': profileMakeStudentMode("modernLiteratureGrade",$columnMD, "","現代文",$scoreValue,-1,""); break;//テストの点数（現代文）を入力するフォームの作成
    case 'oldLiteratureGrade': profileMakeStudentMode("oldLiteratureGrade",$columnMD, "","古文",$scoreValue,-1,""); break;//テストの点数（古文）を入力するフォームの作成
    case 'chineseLiteratureGrade': profileMakeStudentMode("chineseLiteratureGrade",$columnMD, "","漢文",$scoreValue,-1,""); break;//テストの点数（漢文）を入力するフォームの作成
    case 'essayGrade': profileMakeStudentMode("essayGrade",$columnMD, "","小論文",$scoreValue,-1,""); break;//テストの点数（小論文）を入力するフォームの作成
    case 'chemistryGrade': profileMakeStudentMode("chemistryGrade",$columnMD, "","化学",$scoreValue,-1,""); break;//テストの点数（化学）を入力するフォームの作成
    case 'physicsGrade': profileMakeStudentMode("physicsGrade",$columnMD, "","物理",$scoreValue,-1,""); break;//テストの点数（物理）を入力するフォームの作成
    case 'biologyGrade': profileMakeStudentMode("biologyGrade",$columnMD, "","生物",$scoreValue,-1,""); break;//テストの点数（生物）を入力するフォームの作成
    case 'geologyGrade': profileMakeStudentMode("geologyGrade",$columnMD, "","地学",$scoreValue,-1,""); break;//テストの点数（地学）を入力するフォームの作成
    case 'geographyGrade': profileMakeStudentMode("geographyGrade",$columnMD, "","地理",$scoreValue,-1,""); break;//テストの点数（地理）を入力するフォームの作成
    case 'jpnHistoryGrade': profileMakeStudentMode("jpnHistoryGrade",$columnMD, "","日本史",$scoreValue,-1,""); break;//テストの点数（日本史）を入力するフォームの作成
    case 'worldHistoryGrade': profileMakeStudentMode("worldHistoryGrade",$columnMD, "","世界史",$scoreValue,-1,""); break;//テストの点数（世界史）を入力するフォームの作成
    case 'ethicsGrade': profileMakeStudentMode("ethicsGrade",$columnMD, "","倫理",$scoreValue,-1,""); break;//テストの点数（倫理）を入力するフォームの作成
    case 'politicalEconomicsGrade': profileMakeStudentMode("politicalEconomicsGrade",$columnMD, "","政経",$scoreValue,-1,""); break;//テストの点数（政経）を入力するフォームの作成
    case 'modernSocietyGrade': profileMakeStudentMode("modernSocietyGrade",$columnMD, "","現代社会",$scoreValue,-1,""); break;//テストの点数（現代社会）を入力するフォームの作成

    case 'psTeacherReport': profileMake("psTeacherReport", "","12", "", "教師レポート", "もう少し頑張りましょう", "","FREE","4","80"); break;//教師レポートを入力するフォームの作成
    case 'inputMoneyTime': profileMake("inputMoneyTime", "","12", "", "入金完了時刻", "2018-03-14 10:12:34", "","","",""); break;//入金完了時刻を入力するフォームの作成


    case 'sex':
      $selectData = array("男","女");
      profileMakeSelections("sex", "12","性別",$selectData); break;//性別を選択するフォームの作成

    case 'travelTime':
      $selectData = array("遠くても良い","40分以内","20分以内");
      profileMakeSelections("travelTime", "12","移動時間",$selectData); break;//移動時間を選択するフォームの作成

    case 'allianceSchoolIntro':
      $selectData = array("はい","いいえ");
      profileMakeSelections("allianceSchoolIntro", "12","提携塾案件の紹介",$selectData); break;//提携塾案件の紹介を選択するフォームの作成

    case 'seInfoGet':
      $selectData = array("受け取る","受け取らない");
      profileMakeSelections("seInfoGet", "12","SEからのお知らせ",$selectData); break;//SEからのお知らせを選択するフォームの作成

    case 'course':
      $selectData = array("基礎学力コース（小）","中学校受験コース","基礎学力コース（中）","高校受験コース","通常コース","大学受験コース","社会人・浪人生コース");
      profileMakeSelections("course", "12","受講コース",$selectData); break;//受講コースを選択するフォームの作成

    case 'studentCheck':
      $selectData = array("OK","NG");
      profileMakeSelections("studentCheck", "12","生徒確認",$selectData); break;//生徒確認を選択するフォームの作成

    case 'academicCode':
      $selectData = array("小","中","高","社浪");
      profileMakeSelections("academicCode", "12","学校種別",$selectData); break;//学校種別の紹介を選択するフォームの作成

    case 'inputMoney':
      $selectData = array("有","無");
      profileMakeSelections("inputMoney", "12","入金の有無",$selectData); break;//入金の有無を選択するフォームの作成



    case 'transportation':
      $selectData = array("自動車","バイク","原付","自転車");
      profileCheckBoxMakeEX("transportation", "12", "交通手段",$selectData, "", 4); break;//交通手段を入力するフォームの作成

    case 'favoriteSubject':
      $selectData = array("算数","国-小","理-小","社-小","英-中","数学","国-中","理-中","社-中","英-高","数ⅠA","数ⅡB","数ⅢC",
      "現代文","古文","漢文","小論文","化学","物理","生物","地学","地理","日本史","世界史","倫理","政経","現社");
      profileCheckBoxMakeEX("favoriteSubject", "12", "指導可能科目",$selectData, "", 6); break;//指導可能科目を入力するフォームの作成

    case 'availableDays':
      $selectData = array("月","火","水","木","金","土","日");
      profileCheckBoxMakeEX("availableDays", "12", "指導可能曜日",$selectData, "", 7); break;//指導可能曜日を入力するフォームの作成

    case 'teachingLevel':
      $selectData = array("最上位","上位","中位","下位");
      profileCheckBoxMakeEX("teachingLevel", "12", "指導可能レベル",$selectData, "", 4); break;//指導可能レベルを入力するフォームの作成

  }
}

function addSearchForm2($tableName,$count){
  $selectTab = "";
  $selectFirst = '<select name="column['.$count.']" id="column" class="CO" required>';
  $selectTypeFirst = '<select name="searchType['.$count.']" id="searchType" class="ST" required>';
  $selectCenter = '';
  $selectEnd = '</select>';

  echo '<div id="searchMenu" class="col-md-6">';

  //全体条件（AND、OR）
  $selectData = array("全体的AND条件","全体的OR条件","部分的AND条件","部分的OR条件");
  $so = 'searchOption['.$count.']';
  profileMakeSelectionsEX($so, "searchOption","SO","12","論理条件",$selectData,"required");

  //カラム名のoptionタグ出力（新しいテーブルを追加したらここにその情報を追加してください）
  switch($tableName){
    case TABLENAME_LIST[0]:
      $valData = array_values(TEACHER_INFO_ASSOARRAY); $valKey = array_keys(TEACHER_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[1]:
      $valData = array_values(CUSTOMER_INPUT_MONEY_INFO_ASSOARRAY); $valKey = array_keys(CUSTOMER_INPUT_MONEY_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[2]:
      $valData = array_values(PROSPECT_CUSTOMER_INFO_ASSOARRAY); $valKey = array_keys(PROSPECT_CUSTOMER_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[3]:
      $valData = array_values(CUSTOMER_INFO_ASSOARRAY); $valKey = array_keys(CUSTOMER_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[4]:
      $valData = array_values(PS_GRADE_MANAGEMENT_ASSOARRAY); $valKey = array_keys(PS_GRADE_MANAGEMENT_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[5]:
      $valData = array_values(JHS_GRADE_MANAGEMENT_ASSOARRAY); $valKey = array_keys(JHS_GRADE_MANAGEMENT_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[6]:
      $valData = array_values(STUDENT_INFO_ASSOARRAY); $valKey = array_keys(STUDENT_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[7]:
      $valData = array_values(MASTER_INFO_ASSOARRAY); $valKey = array_keys(MASTER_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[8]:
      $valData = array_values(CHARGE_STUDENT_INFO_ASSOARRAY); $valKey = array_keys(CHARGE_STUDENT_INFO_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
    case TABLENAME_LIST[9]:
      $valData = array_values(HS_GRADE_MANAGEMENT_ASSOARRAY); $valKey = array_keys(HS_GRADE_MANAGEMENT_ASSOARRAY);
      profileMakeSelections2EX('column['.$count.']', "column","CO","12","カラム名",$valKey, $valData,"required"); break;
  }

  //値のフォーム
  profileMakeEX('setValue['.$count.']', "setValue","SV","", "12", "", "値", "具体的な値", "", "", "", "","");

  //サーチタイプ（～である、～を含む等）
  $selectType = array("である","で始まる","で終わる","を含む");
  profileMakeSelectionsEX('searchType['.$count.']', "searchType","ST","12","文言条件",$selectType,"required");

  echo inputSearch("検索条件を追加","add").inputSearch("検索条件を削除","sub");
  echo '</div>';
}

function masterFormMake($tableName,$columnList,$count,$type){

  switch($type){
    case "INPUT_FORM":
      echo '<br /><form method="post" style="display: inline">登録フォーム：<br /><div class="row">';
              for($i = 1; $i < count($columnList)-1; $i++) { entryFormMake($columnList[$i]); }
              echo '<div style = "margin:0 auto;" ><input type="hidden" name="tn" size="20" value="'.$tableName.'" readonly style="display: inline"></div></div><br />';
              inputValueSet2("登録する");
      break;

    case "DELETE_FORM":
      echo '<form method="post" style="display: inline">削除フォーム：<br /><div class="row">';
      $selectData = array("no","id");//オプションの選択肢を"NO" or "ID"　に固定
      profileMakeSelections("condition", "12","更新の対象となるNO／ID（chargestudentinfoとprospectcustomerinfoはNOのみで検索）",$selectData);//削除対象となる行の条件（条件　＝　NO or ID）を選択するフォームの作成
      profileMake("del_no", "","12", "", "条件の値", "対象となる条件の値", "","","","");//No
      echo '<div style = "margin:0 auto;" ><input type="hidden" name="tn" size="20" value="'.$tableName.'" readonly style="display: inline"></div></div><br />';
      inputValueSet2("削除する");
      break;

    case "SEARCH_FORM":
      echo '<form method="post" style="display: inline">検索フォーム：<br /><div class="row">';
      addSearchForm2($tableName,$count);
      echo '<div style = "margin:0 auto;" ><input type="hidden" name="tn" size="20" value="'.$tableName.'" readonly style="display: inline"></div></div><br />';
      inputValueSet2("検索する");
      break;

    case "UPDATE_FORM":
      echo '<form method="post" style="display: inline">更新フォーム：<br /><div class="row">';
      $selectData = array("no","id");//オプションの選択肢を"NO" or "ID"　に固定
      profileMakeSelections("conditions0", "12","更新の対象となるNO／ID（chargestudentinfoとprospectcustomerinfoはNOのみで検索）",$selectData);//削除対象となる行の条件（条件　＝　NO or ID）を選択するフォームの作成
      profileMake("condvalue0", "","12", "", "条件の値", "対象となる条件の値", "","","","");//No

        //optionタグ出力（新しいテーブルを追加したらここにその情報を追加してください）
        switch($tableName){
          case TABLENAME_LIST[0]: $valData = array_values(TEACHER_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[1]: $valData = array_values(CUSTOMER_INPUT_MONEY_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[2]: $valData = array_values(PROSPECT_CUSTOMER_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[3]: $valData = array_values(CUSTOMER_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[4]: $valData = array_values(PS_GRADE_MANAGEMENT_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[5]: $valData = array_values(JHS_GRADE_MANAGEMENT_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[6]: $valData = array_values(STUDENT_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[7]: $valData = array_values(MASTER_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[8]: $valData = array_values(CHARGE_STUDENT_INFO_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
          case TABLENAME_LIST[9]: $valData = array_values(HS_GRADE_MANAGEMENT_ASSOARRAY); profileMakeSelections("column0", "12","カラム名",$valData); break;
        }

        profileMake("value0", "","12", "", "値", "対象の値", "","","","");//No
        echo '<div style = "margin:0 auto;" ><input type="hidden" name="tn" size="20" value="'.$tableName.'" readonly style="display: inline"></div></div><br />';
        inputValueSet2("更新する");
      break;
  }
}

function profileMakeStudentMode2($name, $mdValue, $type, $label,$optionValueData, $selectValue){

  $selectFirst = '<select name="'.$name.'" class="form-control" style="border-radius: 0px" >';
  $selectEnd = '</select>';

  $echoStr = $selectFirst.optionValueSetRead("選択してください", "YES");
  for($i = 0; $i < count($optionValueData); $i++){

    //var_dump($optionValueData[$i]);
    //var_dump($selectValue);

    if($selectValue != $optionValueData[$i]) { $echoStr .= optionValueSetRead($optionValueData[$i], ""); }
    else { $echoStr .= optionValueSetSelect($optionValueData[$i], ""); }
  }
  $echoStr .= $selectEnd;

  $textType = "text";
      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>'.$label.'</label><br />'
                .$echoStr
              .'</div>'
            .'</div>';
      }
}

function gradeInputShowMake($subjectName,$subjectColumn){

  //$selectFirst = ""; $selectEnd = "";
  for($h = 0; $h <= 200; $h++){ $scoreValue[] = $h; }
  for($i = 0; $i < count($subjectColumn); $i++){
    if($i%3 == 0) { profileMakeStudentMode($subjectColumn[$i],"4", "",$subjectName[$i],$scoreValue,-1,""); }
    else if($i%3 == 1) { profileMakeStudentMode($subjectColumn[$i],"4", "",$subjectName[$i],$scoreValue,-1,""); }
    else if($i%3 == 2) { profileMakeStudentMode($subjectColumn[$i],"4", "",$subjectName[$i],$scoreValue,-1,""); }
    /*
    $selectFirst = '<br /><select name="'.$subjectColumn[$i].'" >';
    $selectEnd = '</select><br /><br />';
    echo $subjectName[$i].'：'.$selectFirst.optionValueSetInt(0,100).$selectEnd;//各項目の点数を選択するフォームの作成
    */
  }
}

function monthGoalMake($name, $mdValue, $type, $sName, $placeholder, $value, $textStyle, $rows, $cols){
  $textType = "text";
  switch($textStyle){
    case "":
      if($type != ""){
        echo '<div class = "col-md-'.$mdValue.' p'.$type.'-1">'
              .'<div class="form-group">'
                .'<label>担当生徒名：'.$sName.'</label>'
                .'<input type = "'.$textType.'" class = "form-control" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" >'
              .'</div>'
            .'</div>';
      }
      else{
        echo '<div class = "col-md-'.$mdValue.'">'
              .'<div class="form-group">'
                .'<label>担当生徒名：'.$sName.'</label>'
                .'<input type = "'.$textType.'" class = "form-control" style="border-radius: 0px" name = "'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" >'
              .'</div>'
            .'</div>';
      }
      break;


    case "FREE":
      echo '<div class = "col-md-'.$mdValue.'">'
            .'<div class="form-group">'
              .'<label>担当生徒名：'.$sName.'</label>'
              .'<textarea rows= "'.$rows.'" cols="'.$cols.'" class = "form-control" style="border-radius: 0px" name="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" >'.$value.'</textarea>'
            .'</div>'
          .'</div>';
      break;
  }
}

function userModeMake($accessType){
  switch($accessType){
    case 'EX':
      echo '<span class="badge badge-pill badge-danger">EX</span>'; break;
    case 'MGR':
      echo '<span class="badge badge-pill badge-warning">MGR</span>'; break;
    case 'ST':
      echo '<span class="badge badge-pill badge-success">ST</span>'; break;
    case 'ITST':
      echo '<span class="badge badge-pill badge-primary">ITST</span>'; break;

  }
}

function departmentModeMake($department){
  switch($department){
    case 'ICT':
      echo '<span class="badge badge-pill badge-primary">ICT</span>'; break;
    case 'ITM':
      echo '<span class="badge badge-pill badge-primary">ITM</span>'; break;
    case 'PM':
      echo '<span class="badge badge-pill badge-primary">PM</span>'; break;
    case 'ITP':
      echo '<span class="badge badge-pill badge-primary">ITP</span>'; break;
    case 'ITS':
      echo '<span class="badge badge-pill badge-primary">ITS</span>'; break;
    case 'OFFICER':
      echo '<span class="badge badge-pill badge-danger">OFFICER</span>'; break;

  }
}

//サイドバーを作成し表示する関数
function sidebarMake($active,$session){
  $stDis = ""; $mgrDis = ""; $other_IT = "";
  if($session['AT'] == "ST" || $session['AT'] == "ITST") { $stDis = $mgrDis = "DN"; }
  else if($session['AT'] == "MGR") { $mgrDis = "DN"; }
  if($session['AT'] != "ITST") { $other_IT = "DN"; }

  $sd1_1 = ""; $sd2_1 = ""; $sd3_1 = "";
  if($active == "シフト閲覧・出力" || $active == "希望休承認" || $active == "シフト閲覧（部）" || $active == "シフト閲覧（全）") {$sd1_1 = " active";}
  if($active == "交通費清算" || $active == "経費閲覧" || $active == "経費入力・確定") {$sd2_1 = " active"; }
  if($active == "給与明細閲覧" || $active == "給与明細Upload") {$sd3_1 = " active"; }

  $sd1_2 = ""; $sd2_2 = ""; $sd3_2 = "";
  $DN = ' style="display:none;"'; $DB = ' style="display:block;"';
  if($active == "シフト閲覧・出力" || $active == "希望休承認" || $active == "シフト閲覧（部）" || $active == "シフト閲覧（全）") { $sd1_2 = $DB; $sd2_2 = $DN; $sd3_2 = $DN; }
  if($active == "交通費清算" || $active == "経費閲覧" || $active == "経費入力・確定") { $sd1_2 = $DN; $sd2_2 = $DB; $sd3_2 = $DN; }
  if($active == "給与明細閲覧" || $active == "給与明細Upload") { $sd1_2 = $DN; $sd2_2 = $DN; $sd3_2 = $DB; }

  echo '<nav id="sidebar" class="sidebar-wrapper">
          <div class="sidebar-content">

            <!-- 一番上のタイトル入力部分  -->
            <div class="sidebar-brand"><a href="#">axxxis system</a><div id="close-sidebar"><i class="fas fa-times"></i></div></div>

            <!-- 人物情報（サイドバー）  -->
            <div class="sidebar-header">
              <div class="user-pic">
                <img class="img-responsive img-rounded" src="../../EMPLOYEE_IMG/'.$session['ID'].'.jpg" alt="User picture">
              </div>
              <div class="user-info">
                <span class="user-name">'.$session['NAME'].'</span>
                <span class="user-role"></span>'; userModeMake($session['AT']); echo '<p style="height:6px; margin:0;"></p>'; departmentModeMake($session['DM']);
          echo '<span class="user-status"><!-- <i class="fa fa-circle"></i> --><span></span></span>
              </div>
            </div>

            <!-- サイドメニュー全体  -->
            <div class="sidebar-menu">
              <ul>
                <!-- データ管理  -->
                <li class="header-menu"><span>データ管理</span></li>

                <li class="sidebar-dropdown1'.$sd1_1.'">
                  <a href="#"><i class="fas fa-child"></i><span>シフト管理</span></a>
                  <div class="sidebar-submenu1"'.$sd1_2.'>
                    <ul>';
              if($active == "シフト閲覧・出力") {
                echo '<li><a href="./shiftTableShow.php" class="active">シフト閲覧・出力<span class="badge badge-pill badge-success">ST</span></a></li>';
              } else{
                echo '<li><a href="./shiftTableShow.php">シフト閲覧・出力<span class="badge badge-pill badge-success">ST</span></a></li>';
              }
              if($active == "希望休承認") {
                echo '<li class="'.$stDis.'"><a href="./shiftTableApproval.php" class="active">希望休承認<span class="badge badge-pill badge-warning">MGR</span></a></li>';
              } else{
                echo '<li class="'.$stDis.'"><a href="./shiftTableApproval.php">希望休承認<span class="badge badge-pill badge-warning">MGR</span></a></li>';
              }
              if($active == "シフト閲覧（部）") {
                echo '<li class="'.$stDis.'"><a href="#" class="active">シフト閲覧（部）<span class="badge badge-pill badge-warning">MGR</span></a></li>';
              } else{
                echo '<li class="'.$stDis.'"><a href="#">シフト閲覧（部）<span class="badge badge-pill badge-warning">MGR</span></a></li>';
              }
              if($active == "シフト閲覧（全）") {
                echo '<li class="'.$mgrDis.'"><a href="#" class="active">シフト閲覧（全）<span class="badge badge-pill badge-danger">EX</span></a></li>';
              } else{
                echo '<li class="'.$mgrDis.'"><a href="#">シフト閲覧（全）<span class="badge badge-pill badge-danger">EX</span></a></li>';
              }
              echo '</ul>
                  </div>
                </li>

                <li class="sidebar-dropdown2'.$sd2_1.'">
                  <a href="#"><i class="fas fa-chart-pie"></i><span>経費</span></a>
                  <div class="sidebar-submenu2"'.$sd2_2.'>
                    <ul>';
              if($active == "交通費清算"){
                echo '<li><a href="#" class="active">交通費清算<span class="badge badge-pill badge-success">ST・MGR</span></a></li>';
              }else{
                echo '<li><a href="#">交通費清算<span class="badge badge-pill badge-success">ST・MGR</span></a></li>';
              }
              if($active == "経費閲覧"){
                echo '<li class="'.$stDis.'"><a href="#" class="active">経費閲覧<span class="badge badge-pill badge-warning">MGR・EX</span></a></li>';
              }else{
                echo '<li class="'.$stDis.'"><a href="#">経費閲覧<span class="badge badge-pill badge-warning">MGR・EX</span></a></li>';
              }
              if($active == "経費入力・確定"){
                echo '<li class="'.$mgrDis.'"><a href="#" class="active">経費入力・確定<span class="badge badge-pill badge-danger">EX</span></a></li>';
              }else{
                echo '<li class="'.$mgrDis.'"><a href="#">経費入力・確定<span class="badge badge-pill badge-danger">EX</span></a></li>';
              }
              echo '</ul>
                  </div>
                </li>

                <li class="sidebar-dropdown3'.$sd3_1.'">
                  <a href="#"><i class="fas fa-file-pdf"></i><span>給与明細</span></a>
                  <div class="sidebar-submenu3"'.$sd3_2.'>
                    <ul>';
              if($active == "給与明細閲覧"){
                echo '<li><a href="./payslipDownload.php" class="active">給与明細閲覧<span class="badge badge-pill badge-success">ST・MGR</span></a></li>';
              }else{
                echo '<li><a href="./payslipDownload.php">給与明細閲覧<span class="badge badge-pill badge-success">ST・MGR</span></a></li>';
              }
              if($active == "給与明細Upload"){
                echo '<li class="'.$mgrDis.'"><a href="./payslipUpload.php" class="active">給与明細Upload<span class="badge badge-pill badge-danger">EX</span></a></li>';
              }else{
                echo '<li class="'.$mgrDis.'"><a href="./payslipUpload.php">給与明細Upload<span class="badge badge-pill badge-danger">EX</span></a></li>';
              }

              echo '</ul>
                  </div>
                </li>

                <!-- 社員情報  -->
                <li class="header-menu"><span>社員情報</span></li>';

        if($active == "IT学習"){
          echo '<li class="'.$other_IT.'"><a href="#" class="active"><i class="fa fa-book"></i><span>IT学習</span><span class="badge badge-pill badge-primary">ITST</span></a></li>';
        }else{
          echo '<li class="'.$other_IT.'"><a href="#"><i class="fa fa-book"></i><span>IT学習</span><span class="badge badge-pill badge-primary">ITST</span></a></li>';
        }
        if($active == "タイムシート閲覧"){
          echo '<li class="'.$mgrDis.'"><a href="./timeSheetShow.php" class="active"><i class="fas fa-file-alt"></i><span>タイムシート閲覧</span><span class="badge badge-pill badge-danger">EX</span></a></li>';
        }else{
          echo '<li class="'.$mgrDis.'"><a href="./timeSheetShow.php"><i class="fas fa-file-alt"></i><span>タイムシート閲覧</span><span class="badge badge-pill badge-danger">EX</span></a></li>';
        }
        if($active == "プロフィール"){
          echo '<li class="active"><a href="./employeeInfo.php" class="active"><i class="fas fa-address-card"></i><span>プロフィール</span><span class="badge badge-pill badge-success"></span></a></li>';
        }else{
          echo '<li class="active"><a href="./employeeInfo.php"><i class="fas fa-address-card"></i><span>プロフィール</span><span class="badge badge-pill badge-success"></span></a></li>';
        }

        if($active == "プロフ（全）"){
          echo '<li class="'.$mgrDis.'"><a href="./allEmployeeInfo.php" class="active"><i class="fas fa-address-card"></i><span>プロフ（全）</span><span class="badge badge-pill badge-danger">EX</span></a></li>';
        }else{
          echo '<li class="'.$mgrDis.'"><a href="./allEmployeeInfo.php"><i class="fas fa-address-card"></i><span>プロフ（全）</span><span class="badge badge-pill badge-danger">EX</span></a></li>';
        }

        echo '</ul>
            </div>

          </div>

          <!-- サイドバーの一番下の部分  -->
          <div class="sidebar-footer">
            <!--
            <a href="#"><i class="fa fa-bell"></i><!--<span class="badge badge-pill badge-warning notification">3</span>--</a>
            <a href="#"><i class="fa fa-envelope"></i><!--<span class="badge badge-pill badge-success notification">7</span>--</a>
            <a href="#"><i class="fa fa-cog"></i><!--<span class="badge-sonar"></span>--</a>-->
            <a href="../../login.php"><i class="fa fa-power-off"></i></a>
          </div>
        </nav>';
}

//曜日の行
function day_of_the_week_Show(){
  $DotW = ['日','月','火','水','木','金','土'];
  echo '<thead>
          <tr class="hd-ds">';
  for($i = 0; $i < count($DotW); $i++){ echo '<td class="hdd">'.$DotW[$i].'</td>'; }
    echo '</tr>
        </thead>';
}

function clndrMakeShow($year, $month, $last_day){
  $calendar = array(); $j = 0;

  // 月末日までループ
  for ($i = 1; $i < $last_day + 1; $i++) {
    $week = date('w', mktime(0, 0, 0, $month, $i, $year));// 曜日と相互変換可能なIDを取得（$week == 0 -> 日曜日, $week == 1 -> 月曜日, ・・・）
    //var_dump($week);

    if ($i == 1) { for ($s = 1; $s <= $week; $s++) { $calendar[$j]['day'] = ''; $j++; /* 1日目の曜日までをループし、前半に空文字をセット */ } }//1日の場合
    $calendar[$j]['day'] = $i; $j++;// 配列に日付をセットし、格納する配列番号（"日にち - 1" に相当）を１つずらす。
    if ($i == $last_day) { for ($e = 1; $e <= 6 - $week; $e++) { $calendar[$j]['day'] = ''; $j++; /*月末日から残りをループし、後半に空文字をセット*/ } }//月末日の場合
  }
  //var_dump($calendar);
  return $calendar;
}

function week_calendar_num_show($clndr,$start_num){ echo '<tr>'; for($i = 0;  $i < 7; $i++){ echo '<td>'.$clndr[$start_num * 7 + $i]['day'].'</td>'; } echo '</tr>'; }
function week_calendar_schedule_show($clndr, $session, $start_num){
  echo '<tr>';

  $tonality_num = 0;
  while($clndr[$tonality_num]['day'] == ''){ $tonality_num++; }
  //var_dump($tonality_num);
  //var_dump($clndr);
  //var_dump($_SESSION);
  for($i = 0;  $i < 7; $i++){
    $temp = $start_num * 7 + $i;
    $temp2 = $temp - $tonality_num + 1;
    $name = "day".$temp2;

    $value = "";
  if(isset($session['AD']) && $clndr[$temp]['day'] != '' && $temp2 >= 1 && $temp2 < 32) { $value = $session['AD'][$name]; /*var_dump($value);*/ }
    echo '<td>'; if($clndr[$temp]['day'] != ''){ profileMakePulldownMode($name, "", "12", "", "", PULLDOWNLIST_ATTENDANCE, $value, ""); }
    echo '</td>';
  }

  echo '</tr>';
}

function week_calendar_decision_schedule_show($clndr, $session, $start_num){
  echo '<tr>';

  $tonality_num = 0;
  while($clndr[$tonality_num]['day'] == ''){ $tonality_num++; }
  //var_dump($tonality_num);
  //var_dump($clndr);

  for($i = 0;  $i < 7; $i++){
    $temp = $start_num * 7 + $i;
    $temp2 = $temp - $tonality_num + 1;
    $name = "day".$temp2;
    echo '<td>'; if($clndr[$temp]['day'] != ''){ if(isset($_SESSION['AD']) && $_SESSION['AD']['mgrOK'] == 'OK'){ echo $_SESSION['AD'][$name]; } }
    echo '</td>';
  }

  echo '</tr>';
}

//カレンダーを作成し、表示する関数
function holidayClndrShowMake($year,$month, $session){

  //初期準備
  //$y = date('Y'); $m = date('n');//現在の年月を取得
  $last_day = date('j', mktime(0, 0, 0, $month + 1, 0, $year));// 月末日を取得

  $calendar = array(); $calendar = clndrMakeShow($year, $month, $last_day);
  //var_dump($calendar);

  $week_num = count($calendar) / 7;//該当月のカレンダーに、何週分を表示するかをここで計算
  //var_dump($week_num);


  echo '<div id = "cld" style="overflow-x: auto;">
          <div class = "cld-ctls">
            <form action="./shiftTableShow.php" name="attendanceForm">
              <div class="mnh">'.$year.'年'.$month.'月</div>
              <table class="clndr-table" style="min-width:750px;">';
          echo '<tbody>'; day_of_the_week_Show(); for($i = 0; $i < $week_num; $i++ ) { week_calendar_num_show($calendar,$i); week_calendar_schedule_show($calendar, $session, $i); }
          echo '</tbody>
              </table>
              <input name="year" type="hidden" value="'.$year.'">
              <input name="month" type="hidden" value="'.$month.'">
              <center><input type="submit" class = "btn btn-blueVer rounded-pill" name="attendanceUpdate" value="希望休を送信する"></center>
            </form>
          </div>
        </div>';
}

//カレンダーを作成し、表示する関数
function holidayClndrDecisionShowMake($year, $month, $session){
  //var_dump($session);

  //初期準備
  //$y = date('Y'); $m = date('n');//現在の年月を取得
  $last_day = date('j', mktime(0, 0, 0, $month + 1, 0, $year));// 月末日を取得

  $calendar = array(); $calendar = clndrMakeShow($year, $month, $last_day);
  //var_dump($calendar);

  $week_num = count($calendar) / 7;//該当月のカレンダーに、何週分を表示するかをここで計算
  //var_dump($week_num);


  echo '<div id = "cld" style="overflow-x: auto;">
          <div class = "cld-ctls">
            <div class="mnh">'.$year.'年'.$month.'月</div>
            <table class="clndr-table" style="min-width:750px;">';
        echo '<tbody>'; day_of_the_week_Show(); for($i = 0; $i < $week_num; $i++ ) { week_calendar_num_show($calendar,$i); week_calendar_decision_schedule_show($calendar, $session, $i); }
        echo '</tbody>
            </table>
          </div>
        </div>';
}

//連想配列のキーを対応するラベル名に変換して返す関数
function fromKeytoLabel($key, $keyDB, $labelDB){
  $label = array();
  for($i = 0; $i < count($key); $i++){
    if($key[$i] == $keyDB[$i]){ $label[] = $labelDB[$i]; }
  }
  return $label;
}

//DBから引っ張ってきた連想配列に関して、指定したキー群（および値群）を削除して返す関数
function unsetExe(&$row,$unsetKey){
  for($i = 0; $i < count($unsetKey); $i++){
    if(isset($row[$unsetKey[$i]])) { unset($row[$unsetKey[$i]]); }
  }
}

//全社員のプロフィール情報を表示する関数
function allEmployeeProfileShow($row){
  for($h = 0; $h < count($row); $h++) { unsetExe($row[$h], USEFUL_UNSET_KEY); }//必要のない項目を予め削除しておく

  echo '<div id = "AEP" class="scrX cent">
          <div class = "AEP-ctls">
            <div class="title">全社員のプロフィール一覧</div>';


  for($i = 0; $i < count($row); $i++){
      echo '<br /><table class="table table-striped table-dark" style="width:100%; min-width:300px; max-width:1200px; align:center;">
              <tbody>';
    $arrayKey = array(); $arrayVal = array();
    foreach($row[$i] as $key => $value){ $arrayKey[] = $key; $arrayVal[] = $value; }
    //var_dump($arrayKey); var_dump($arrayVal);
    $label = array(); $label = fromKeytoLabel($arrayKey, EMPLOYEE_INFO_TABLE_KEY, EMPLOYEE_INFO_TABLE_LABEL);
  for($j = 0; $j < count($label); $j++) { echo '<tr><td>'.$label[$j].'</td><td>'.$arrayVal[$j].'</td></tr>'; }
        echo '</tbody>
            </table><br />';
  }

    echo '</div>
        </div>';
}
?>
